# Docker Image for Archival Dataset Attention model training

## Generate image
``` bash
docker image build --file Dockerfile - archival  workdir
```

## Run container with image
Create a container to run the image created above, bound to a volume located in /[workdir]/app in OS, and in /app in container

**Windows** (Docker Desktop with WSL2)
``` bash
docker run --rm -it --env NVIDIA_DISABLE_REQUIRE=1  --gpus all -v ${PWD}/app:/app archival
```
**Linux and Mac**
``` bash
docker run --rm -it --gpus all -v $(pwd)/app:/app archival
```

**These commands will run the default command of the image, in this case, the entrypoint.sh script.**
To override this command on container run, just add the command at the end

*Example :*
``` bash
docker run --rm -it --gpus all -v $(pwd)/app:/app archival python3 train_archival.py -n 32 -e 50 -b 16 --suffix mymodel
```

