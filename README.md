<!-- PROJECT SHIELDS -->

<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
*** Original template: https://raw.githubusercontent.com/othneildrew/Best-README-Template/master/README.md
-->


[![Contributors][contributors-shield]][contributors-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="#">
    <img src="readme/logo-irisa-main.png" alt="Logo" height="80">
  </a>

  <h1 align="center">FrNewslink</h1>

  <p align="center">
    Using Attention mechanism to justify document linking
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details open="close">
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->
## About The Project

Regrouping the Languagues treatments

### Built With

The project uses the following frameworks:
* [Spacy.io](https://spacy.io)
* [Pytorch](https://pytorch.org/)
* [Keras](https://keras.io/)

<!-- GETTING STARTED -->
## Getting Started

### Pip (recommended)

1. Choose place for virtual environment. Here we the venv named _nlp_

```bash
VENV=/path/to/virtualenv
python -m venv $VENV/nlp
source $VENV/nlp/bin/activate
```

2a. Install manually by `pip` command.

CPU:
```bash
source $VENV/nlp/bin/activate
pip install torch torchtext pandas scikit-learn torchmetrics ray ray[tune] tensorboard torch-summary spacy matplotlib seaborn torchinfo --no-cache-dir
pip install ipywidgets jupyterlab --no-cache-dir
```

GPU:
```bash
source $VENV/nlp/bin/activate
pip install torch torchtext pandas scikit-learn torchmetrics ray ray[tune] tensorboard torch-summary matplotlib seaborn torchinfo --no-cache-dir
pip install -U 'spacy[cuda113]'
pip install ipywidgets jupyterlab --no-cache-dir
```

2b. Install from `requirement.txt`. Don't forgcet `--no-cache-dir` when in cluster
```bash
source $VENV/nlp/bin/activate
pip install -r requirement.txt --no-cache-dir
```

### Conda

1. Installation:
Supposing you want to install a specific version of conda `<conda_installer>`

```bash
source ~/.bashrc
curl -O <conda_installer>.sh
bash <conda_installer>.sh
```
   
2a. Restoring conda environment
```bash
source ~/.bashrc
conda env create -f nlp.yml 
conda activate nlp
```

2b. Or manually recreate conda environment:

```bash
conda create -n nlp python=3.8.5 pandas scikit-learn matplotlib seaborn tensorboard spacy tabulate
conda activate nlp
conda install pytorch cudatoolkit=11.1 -c pytorch -c conda-forge -c nvidia
conda install -c pytorch torchtext=0.9
conda install -c conda-forge torchmetrics ray-tune tensorboardx ipywidgets
pip install torch-summary
jupyter nbextension enable --py widgetsnbextension
```

See more for installing [GPU Pytorch](https://pytorch.org/get-started/locally/)

### Spacy

Install required `spacy` model:

```bash
python -m spacy download en_core_web_md
python -m spacy download fr_core_news_md
```

## IGRIDA

Full documentation of [IGRIDA](https://igrida.gitlabpages.inria.fr/userdocs/)

### 1. Open connection

```bash
ssh ${USER}@igrida-frontend.irisa.fr
```

### 2. Managing data files in cluster


1. By mounting a virual disk

Use `sshfs` in Mac each time turn on the pc. In this example, 
we mount virtual disk at `$HOME/Projects/IGRIDA/RUNS` that sync 
to `$RUNDIR` in distant server:

```bash
mkdir -p $HOME/Projects/IGRIDA/RUNS
sshfs $USER@igrida-frontend.irisa.fr:/srv/tempdd/$USER/RUNS $HOME/Projects/IGRIDA/RUNS
```


2. Manual

**Transfer to server**, for example data folder `dataset` to `/srv/tempdd/$USER/DATASET/`.
Note that this operation will resulted in having `/srv/tempdd/$USER/DATASET/dataset`

```bash
rsync -avz <dataset> ${USER}@igrida-frontend.irisa.fr:/srv/tempdd/$USER/DATASET 
```

**Download from server**
```bash
scp -r ${USER}@igrida-frontend.irisa.fr:/udd/${USER}/<a directory> <my local directory> 
```

### 3. Run script

1. Interactive mode

```bash
cd nlp/src
oarsub -I -l {"gpu_mem >= 32"}/gpu_device=1,walltime=5:00:00
module load spack/cuda/11.3.1
module load spack/cudnn/8.0.4.30-11.0-linux-x64
conda activate nlp
python train.py -d $HOME/dataset/nlp -o $RUNDIR -e 100 -n 1000
```

Order a precific server

```bash
oarsub -I -l {"host='igrida-abacus3.irisa.fr' OR host='igrida-abacus10.irisa.fr' OR host='igrida-abacus3.irisa.fr' OR host='igrida-abacus13.irisa.fr' OR host='igrida-abacus14.irisa.fr' OR host='igrida-abacus18.irisa.fr'"}/gpu_device=1,walltime=5:00:00
```

2. Submitting the script

```bash
oarsub -S ./train.sh
```

Script template:

```sh
#!/usr/bin/env bash
#OAR -l {gpu_mem >= 32}/gpu_device=1,walltime=48:00:00
#OAR -O /srv/tempdd/dunguyen/RUNS/out.%jobid%.log
#OAR -E /srv/tempdd/dunguyen/RUNS/err.%jobid%.log

#patch to be aware of "module" inside a job
. /etc/profile.d/modules.sh

# Decomment the following line to see which command has been called in detail
# set -xv

module load spack/cuda/11.3.1
module load spack/cudnn/8.0.4.30-11.0-linux-x64

source $VENV/nlp/bin/activate

EXEC_FILE=src/train.py
DATASET=$DATADIR/snli

echo
echo "=============== RUN ${OAR_JOB_ID} ==============="
echo "Run $EXEC_FILE"
python $EXEC_FILE --data $DATASET --OAR_ID $OAR_JOB_ID -e 1 -b 32 --cache $RUNDIR
echo "Done"
```

<!-- CONTACT -->
## Contact

Duc Hau NGUYEN - [in/duc-hau-nguyen](https://www.linkedin.com/in/duc-hau-nguyen/) - duc-hau.nguyen@irisa.fr


<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.

<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://img.shields.io/github/contributors/othneildrew/Best-README-Template.svg?style=for-the-badge
[contributors-url]: https://gitlab.irisa.fr/dunguyen/nlp/-/graphs/master
[forks-shield]: https://img.shields.io/github/forks/othneildrew/Best-README-Template.svg?style=for-the-badge
[forks-url]: https://github.com/othneildrew/Best-README-Template/network/members
[stars-shield]: https://img.shields.io/github/stars/othneildrew/Best-README-Template.svg?style=for-the-badge
[stars-url]: https://github.com/othneildrew/Best-README-Template/stargazers
[issues-shield]: https://img.shields.io/github/issues/othneildrew/Best-README-Template.svg?style=for-the-badge
[issues-url]: https://github.com/othneildrew/Best-README-Template/issues
[license-shield]: https://img.shields.io/github/license/othneildrew/Best-README-Template.svg?style=for-the-badge
[license-url]: https://github.com/othneildrew/Best-README-Template/blob/master/LICENSE.txt
[linkedin-shield]: https://img.shields.io/badge/-LinkedIn-black.svg?style=for-the-badge&logo=linkedin&colorB=555
[linkedin-url]: https://www.linkedin.com/in/duc-hau-nguyen/
[product-screenshot]: images/screenshot.png
