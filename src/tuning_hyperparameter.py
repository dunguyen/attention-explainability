import multiprocessing
import os
from os import path
from argparse import ArgumentParser

import ray
import spacy

from torch.utils.data import DataLoader

from data.collate import StandardCollate
from data.snli.dataset import SNLIDataset, build_vocab, load_vocab
import torch
from torch import nn, optim

from helpers import pprint, env
from helpers.logger import init_logging, log
from trainer import evaluate, fit
from model.alpha import AlphaModel, EmbeddingLanguageModel
from data.pipeline import *

from ray import tune
from ray.tune import CLIReporter
from ray.tune.schedulers import ASHAScheduler

MODE_DEV = 'dev'
MODE_EXP = 'exp'
MODE_DEBUG = 'debug'

def get_config(mode):
	
	log.debug(f'Config at mode: {mode}')
	
	if mode in [MODE_DEV, MODE_DEBUG]:
		
		return {
			# model config
			'd_embedding': tune.grid_search([100, 300]),
			
			'n_lstm': tune.grid_search([1]),
			'd_hidden_lstm': tune.grid_search([-1, 100]),
			'activation': tune.choice(['relu']),
			'n_fc_out': tune.randint(0, 1),
			'd_fc_out': tune.grid_search([-1, 100]),
	
			# learning config
			'dropout': tune.grid_search([0.1]),
			'lr': tune.loguniform(1e-4, 1e-4),
			
			# preprocess config
			'pipeline': tune.choice(['lower'])
		}
		
	if mode == MODE_EXP:
		return {
			# model config
			# 'd_embedding': tune.grid_search([100, 300]),
			'd_embedding': tune.grid_search(list(VECTOR_BY_DIM.keys())),
			
			
			'n_lstm': tune.grid_search([1, 2]),
			'd_hidden_lstm': tune.grid_search([-1, 100, 200, 300]),
			'activation': tune.choice(['relu', 'tanh']),
			'n_fc_out': tune.randint(0, 5),
			'd_fc_out': tune.grid_search([-1, 100, 200, 300]),
	
			# learning config
			'dropout': tune.grid_search([0, 0.05, 0.1, 0.2, 0.5]),
			'lr': tune.loguniform(1e-5, 1e-1),
			
			# preprocess config
			'pipeline': tune.choice(list(PIPELINE.keys()))
		}

	raise AttributeError(f'Unknown mode: {mode}')


# Dictionary
VECTOR_BY_DIM = {
	50: 'glove.6B.50d',
	100: 'glove.6B.100d',
	200: 'glove.6B.200d',
	300: 'glove.840B.300d'
}

PIPELINE = {
	'standard': TextPipeline,
	'lemma': LemmaPipeline,
	'lower': CaseInsPipeline,
	'lemma-lower': LemmaCasePipeline
}

def train(config, checkpoint_dir=None, params=None, context=None):
	
	env.disable_tqdm = params.get('OAR_ID', None) is not None
	log.info('OAR_ID exist, turn off progress bar')
	device = "cuda:0" if torch.cuda.is_available() else "cpu"
	
	spacy_model = spacy.load('en_core_web_md')
	vectors_by_dim = context['vectors_by_dim']
	
	trainset = SNLIDataset('train', params['cache'], n=params['nb_data_max'], shuffle=True)
	valset = SNLIDataset('dev', params['cache'], n=params['nb_data_max'], shuffle=False)
	
	text_pipeline = PIPELINE[config['pipeline']](spacy_model)
	vectors = vectors_by_dim.get(config['d_embedding'])
	vocab = build_vocab(trainset, vectors=vectors, cache_path=params['cache'], pipeline=text_pipeline)
	numeric_pipeline = text_pipeline.numericalizer(vocab)
	collate_fn = StandardCollate(padding=vocab['<pad>'], pipeline=numeric_pipeline, multiclass=True, device=device)
	
	# load data into batch
	train_generator = DataLoader(trainset, collate_fn=collate_fn, batch_size=params['batch_size'], shuffle=True)
	val_generator = DataLoader(valset, collate_fn=collate_fn, batch_size=params['batch_size'], shuffle=False)
	
	lm = EmbeddingLanguageModel(vocab=vocab, d_embedding=config['d_embedding'])
	model = AlphaModel(vocab=vocab, language_model=lm,
					   n_lstm=config['n_lstm'],
					   d_hidden_lstm=config['d_hidden_lstm'],
					   activation=config['activation'],
					   n_fc_out=config['n_fc_out'],
					   d_fc_out=config['d_fc_out'],
					   dropout=config['dropout'])
	
	if torch.cuda.is_available() and torch.cuda.device_count() > 1:
		model = nn.DataParallel(model)
	model.to(device)
	
	# learning configuration
	optimizer = optim.Adam(model.parameters(), lr=config['lr'])
	criterion = nn.CrossEntropyLoss()
	
	if checkpoint_dir:
		log.warning('load from previous checkpoint')
		model_state, optimizer_state = torch.load(os.path.join(checkpoint_dir, "checkpoint"))
		model.load_state_dict(model_state)
		optimizer.load_state_dict(optimizer_state)
	
	def callback(report: dict):
		with tune.checkpoint_dir(report['epoch']) as checkpoint_dir:
			path = os.path.join(checkpoint_dir, "checkpoint")
			torch.save((model.state_dict(), optimizer.state_dict()), path)
		
		tune.report(train_loss=report['train']['loss'],
					train_accuracy=report['train']['accuracy'],
					val_loss=report['validation']['loss'],
					val_accuracy=report['validation']['accuracy'])
		
	fit(model, optimizer=optimizer, loss_fn=criterion, device=device, epoch=params['epoch'],
					 generator=(train_generator, val_generator), continuous=False, callback=callback, clear_cuda=params['clear_cuda'])

def parse_argument():
	"""
	Get parameters from console
	Returns:
		dict
	"""
	parser = ArgumentParser()
	parser.add_argument('--name', type=str,
						help='Experiment name')
	parser.add_argument('--cache', '-c', type=str)
	parser.add_argument('--ray_tmp', type=str)
	parser.add_argument('--mode', type=str, default=MODE_DEV,
						help=f'Mode of experimentation ({MODE_DEV}, {MODE_EXP}, {MODE_DEBUG})')
	parser.add_argument('--nb_data_max', '-n', type=int, default=-1)
	parser.add_argument('--batch_size', '-b', type=int, default=-1,
						help='Number of data in batch. Default: 32')
	parser.add_argument('--OAR_ID', type=int,
						help='Indicate whether we are in IGRIDA cluster mode')
	parser.add_argument('--epoch', '-e', type=int, default=1,
						help='Number training epoch. Default: 1')
	parser.add_argument('--clear_cuda', action='store_true')
	parser.add_argument('--resume', action='store_true')
	parser.add_argument('--fail_fast', action='store_true')
	
	parser.add_argument('--cpu', type=int,
						help='CPU limit given to ray')
	
	params = vars(parser.parse_args())
	pprint('=== Parameters ===', params)
	params = {k: v for k, v in params.items() if v is not None}
	
	params['mode'] = params['mode'].lower()
	
	return params

if __name__ == '__main__':
	
	init_logging()
	params = parse_argument()
	
	RAY_ROOT_PATH = path.join(params['cache'], 'ray_tune')
	RAY_LOG_PATH = path.join(RAY_ROOT_PATH, 'log')
	RAY_RESULT_PATH = path.join(RAY_ROOT_PATH, 'result')
	
	# by default use all cpu
	cpus_per_trial = 1
	gpus_per_trial = 1 if torch.cuda.is_available() else 0
	
	log.info(f'#Available GPUs: {torch.cuda.device_count()} | CPUS: {multiprocessing.cpu_count()}')
	
	config = get_config(mode=params['mode'])
	
	context = {
		'vectors_by_dim': VECTOR_BY_DIM,
	}
	
	# Initialize ray environment
	ray.init(num_cpus=params.get('cpu', None),
		_temp_dir=params.get('ray_tmp', None),
		local_mode=params['mode'] == MODE_DEBUG)
	
	scheduler = ASHAScheduler(
		metric="val_loss",
		mode="min",
		max_t=params['epoch'],
		grace_period=1,
		reduction_factor=2)
	
	reporter = CLIReporter(
		# parameter_columns=["l1", "l2", "lr", "batch_size"],
		metric_columns=['train_loss', 'train_accuracy', 'val_loss', 'val_accuracy', 'training_iteration'])
	
	resuming_path = path.join(RAY_LOG_PATH, params.get('name', 'test'))
	soft_resume = path.exists(resuming_path) and params.get('resume', False) # resume from the last checkpoint if exist
	
	analysis = tune.run(
		tune.with_parameters(train, params=params, context=context),
		local_dir=RAY_LOG_PATH,
		resources_per_trial={'cpu': cpus_per_trial, 'gpu': gpus_per_trial},
		stop={'training_iteration': params['epoch']},
		fail_fast=params['fail_fast'],  # Stop when first failure
		config=config,
		num_samples=3,
		scheduler=scheduler,
		progress_reporter=reporter,
		name=params.get('name', 'Unnamed'),
		resume=soft_resume
	)
	
	os.makedirs(RAY_RESULT_PATH)
	analysis_path = path.join(RAY_RESULT_PATH, f'reporting_{params["name"]}.csv')
	analysis.results_df.to_csv(analysis_path, index=False)
	
	best_trial = analysis.get_best_trial("val_loss", "min", "last")
	print("Best trial config: {}".format(best_trial.config))
	print("Best trial final validation loss: {}".format(best_trial.last_result["val_loss"]))
	print("Best trial final validation accuracy: {}".format(best_trial.last_result["val_accuracy"]))
	
	spacy_model = spacy.load('en_core_web_md')
	config = best_trial.config
	text_pipeline = PIPELINE[config['pipeline']](spacy_model)
	vocab = load_vocab(cache_path=params['cache'], pipeline=text_pipeline, vectors=VECTOR_BY_DIM.get(config['d_embedding']))
	lm = EmbeddingLanguageModel(vocab=vocab, d_embedding=config['d_embedding'])
	best_trained_model = AlphaModel(vocab=vocab, language_model=lm)
	
	device = "cpu"
	if torch.cuda.is_available():
		device = "cuda:0"
		if gpus_per_trial > 1:
			best_trained_model = nn.DataParallel(best_trained_model)
	best_trained_model.to(device)
	
	best_checkpoint_dir = best_trial.checkpoint.value
	model_state, optimizer_state = torch.load(path.join(best_checkpoint_dir, 'checkpoint'))
	best_trained_model.load_state_dict(model_state)
	
	testset = SNLIDataset('test', params['cache'], params['nb_data_max'])
	collate_fn = StandardCollate(padding=vocab['<pad>'], pipeline=text_pipeline.numericalizer(vocab), multiclass=True, device=device)
	test_generator = DataLoader(testset, collate_fn=collate_fn, batch_size=params['batch_size'], shuffle=False)
	
	criteria = ['accuracy', 'precision', 'recall', 'f1']
	score = evaluate(best_trained_model, test_generator=test_generator, criteria=criteria, device=device)
	print("Best trial test set accuracy: {}".format(score))


