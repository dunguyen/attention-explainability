import torch
from torch import nn
from torch.nn import Parameter


class LuongGeneralScore(nn.Module):

	def __init__(self, hidden_dim):
		super().__init__()
		self.hidden_dim = hidden_dim
		self.weight = Parameter(torch.randn(hidden_dim, hidden_dim), requires_grad=True)

	def forward(self, decoder_outputs, encoder_outputs):
		"""

		Args:
			decoder_outputs: batch_size, decoder length, hidden_size
			encoder_outputs: batch_size, encoder length, hidden_size

		Returns:

		"""
		# L_dec = decoder sequence length
		# L_enc = encoder sequence length
		# N = batch size
		encoder_outputs = encoder_outputs.permute(0, 2, 1)  # encoder_outputs.size() == (N, d_hidden, L_enc)
	
		score = torch.bmm(decoder_outputs @ self.weight, encoder_outputs) # score == (N, L_dec, L_enc)

		return score


if __name__ == '__main__':
	# Unit Test Attention General score
	# L = sequence_length, N = batch_size, d_hidden = embedding_size
	
	# L_enc = 4, L_dec = 2, N = 1, d_hidden = 3
	decoder_outputs = torch.FloatTensor([[1, 2, 3], [4, 5, 7]]).view(1, 2, -1) # size() == (N, L, D)
	encoder_outputs = torch.FloatTensor([[1, 2, 3], [0, 1, 0], [4, 5, 7], [1, 2, 3]]).view(1, 4, -1)

	hidden_dim = decoder_outputs.size(2)

	attn = LuongGeneralScore(hidden_dim)
	score = attn(decoder_outputs, encoder_outputs)
	print('Result: ', score)
	print('Output dim:', score.size())
	print('Expect dim: (N, L_dec, L_enc) = (1, 2, 4)')
	print('=================')
