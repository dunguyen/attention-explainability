import torch
from torch import nn


class LuongDotScore(nn.Module):

	def __init__(self):
		super().__init__()

	def forward(self, decoder_outputs, encoder_outputs):
		"""

		Args:
			decoder_outputs: batch_size, decoder length, hidden_size
			encoder_outputs: batch_size, encoder length, hidden_size

		Returns:

		"""
		# L = sequence length, N = batch size, d_hidden = embedding size

		# decoder_outputs = torch.transpose(decoder_outputs, 0, 1) # decoder_outputs.size() == (N, L_dec, d_hidden)

		# encoder_outputs = encoder_outputs.permute(1, 2, 0) # encoder_outputs.size() == (N, d_hidden, L_enc)
		encoder_outputs = encoder_outputs.permute(0, 2, 1) # encoder_outputs.size() == (N, d_hidden, L_enc)

		score = torch.bmm(decoder_outputs, encoder_outputs) # score == (N, L_dec, L_enc)
		return score


if __name__ == '__main__':
	# Unit Test Attention General score
	# L = sequence_length, N = batch_size, d_hidden = embedding_size
	decoder_outputs = torch.FloatTensor([[1, 2, 3], [4, 5, 7]]).view(1, 2, -1) # size() == (L, N, D)
	encoder_outputs = torch.FloatTensor([[1, 2, 3], [0, 1, 0], [4, 5, 7], [1, 2, 3]]).view(1, 4, -1)

	# L_enc = 4, L_dec = 2, N = 1, d_hidden = 3

	hidden_dim = decoder_outputs.size(2)

	atten = LuongDotScore()
	score = atten(decoder_outputs, encoder_outputs)
	print('Result: ', score)
	print('Output dim:', score.size())
	print('Expect dim: (N, L_dec, L_enc) = (1, 2, 4)')
	print('=================')
