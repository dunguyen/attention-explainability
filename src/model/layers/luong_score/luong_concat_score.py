import torch
from torch import nn
import torch.nn.functional as F
from torch.autograd import Variable


class LuongGeneralScore(nn.Module):

	def __init__(self, hidden_dim, output_dim=None):
		super().__init__()
		self.hidden_dim = hidden_dim
		self.output_dim = output_dim if output_dim is not None else hidden_dim
		self.fc = nn.Linear(2*hidden_dim, self.output_dim)
		self.weight_align = nn.Parameter(torch.FloatTensor(self.output_dim))

	def forward(self, decoder_outputs, encoder_outputs):
		"""

		Args:
			decoder_outputs (query): decoder length, batch_size, hidden_size
			encoder_outputs (key): max input length, batch_size, hidden_size

		Returns:

		"""
		# L = sequence length, N = batch size, d_hidden = embedding size

		decoder_outputs = torch.transpose(decoder_outputs, 0, 1) # decoder_outputs.size() == (N, L_dec, d_hidden)

		encoder_outputs = encoder_outputs.permute(1, 2, 0) # encoder_outputs.size() == (N, d_hidden, L_enc)

		# concat
		for query in decoder_outputs:
			#TODO not finished concat in batch
			pass
		concat = torch.cat([encoder_outputs, decoder_outputs], dim=0)

		score = F.tanh(self.fc(concat))
		score = torch.matmul(score, self.weight_align)

		return score


if __name__ == '__main__':
	# Unit Test Attention General score
	# L = sequence_length, N = batch_size, d_hidden = embedding_size
	decoder_outputs = torch.FloatTensor([[1, 2, 3], [4, 5, 7]]).view(2, 1, -1) # size() == (L, N, D)
	encoder_outputs = torch.FloatTensor([[1, 2, 3], [0, 1, 0], [4, 5, 7], [1, 2, 3]]).view(4, 1, -1)

	# L_enc = 4, L_dec = 2, N = 1, d_hidden = 3

	hidden_dim = decoder_outputs.size(2)

	atten = LuongGeneralScore(hidden_dim)
	score = atten(decoder_outputs, encoder_outputs)
	print('Result: ', score)
	print('Output dim:', score.size())
	print('Expect dim: (N, L_dec, L_enc) = (1, 2, 4)')
	print('=================')
