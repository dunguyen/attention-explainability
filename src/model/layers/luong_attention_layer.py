import torch
from torch import nn
import torch.nn.functional as F

from model.layers.luong_score.luong_dot_score import LuongDotScore
from model.layers.luong_score.luong_general_score import LuongGeneralScore


class LuongAttention(nn.Module):

	def __init__(self, method, hidden_dim):
		"""
		
		Args:
			method:
			hidden_dim:
			normalize: If True, the alignment score will be normalized $score = score / d_K$
			with $d_K$ is the key dimension
		"""
		
		super().__init__()
		self.method = method
		self.hidden_size = hidden_dim

		if method == 'general':
			self.attn_score = LuongGeneralScore(hidden_dim)
		elif method == 'dot':
			self.attn_score = LuongDotScore()
		elif method == 'concat':
			self.attn_score = nn.Linear(self.hidden_size * 2, hidden_dim)
			self.v = nn.Parameter(torch.FloatTensor(1, hidden_dim))

	def forward(self, decoder_outputs, encoder_outputs, decoder_mask=None, encoder_mask=None):
		"""

		Args:
			decoder_outputs: batch size, dec sequence length, hidden_dim
			encoder_outputs: batch size, enc sequence length, hidden_dim
			decoder_mask: batch_size, sequence length
			encoder_mask: batch_size, sequence length
		Returns:
			context_vector: batch_size, 1, hidden_dim
			alignment: batch_size, 1, encoder sequence length
		"""

		# L = sequence_length, N = batch_size, h = hidden_dim
		attn_score = self.attn_score(decoder_outputs, encoder_outputs)  # size() == (N, L_dec, L_enc)

		# attention is all you need: normalize score by
		# /sqrt(d_K) with d_K is key dimension
		dim = torch.tensor([decoder_outputs.size(-1)], dtype=torch.float, device=attn_score._device)
		attn_score = torch.div(attn_score, torch.sqrt(dim))

		alignment = F.softmax(attn_score, dim=2)                        # size() == (N, 1, L_enc)

		# applying mask on attention weights
		if encoder_mask is not None:
			encoder_mask = encoder_mask.unsqueeze(dim=1)                # size() == (N, 1, L_enc)
			alignment = alignment * encoder_mask                        # size() == (N, 1, L_enc)

		# without batch first
		# encoder_outputs = encoder_outputs.permute(1, 0, 2)  # size() == (N, L_enc, h)

		context = torch.bmm(alignment, encoder_outputs)  # size() == (N, 1, h)

		return context, alignment


if __name__ == '__main__':
	# Unit Test Attention Luong
	# L = sequence_length, N = batch_size, d_hidden = embedding_size
	hidden = torch.FloatTensor([1, 2, 3]).view(1, 1, -1) # u.size() == (N, L, D)
	encoder_outputs = torch.FloatTensor([[1, 2, 3], [0, 1, 0], [4, 5, 7], [1, 2, 3]]).view(1, 4, -1)

	# L_enc = 4, N = 1, d_hidden = 3
	hidden_dim = hidden.size(2)
	for mode in ['general', 'dot']:
		atten = LuongAttention(mode, hidden_dim)
		score = atten(hidden, encoder_outputs)
		print('Mode = ', mode)
		print('Result: ', score)
		print('=================')
