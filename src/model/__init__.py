from abc import ABC
from torch import nn


class Net(nn.Module, ABC):
	
	def __init__(self):
		super().__init__()
		self._n_class = 1
	
	@property
	def n_class(self):
		return self._n_class
	
	@n_class.setter
	def n_class(self, n_class):
		self._n_class = n_class
		
	def __str__(self):
		return self.__class__.__name__