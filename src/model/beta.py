import logging

import torch
from torch import nn
import torch.optim as optim
from torchtext.vocab import Vocab

from model import Net


class BetaModel(Net):
    
    def __init__(self, **kwargs):
        """
		This model does not include embedding layer, in order to give freedom to vectorizer choice
		Args:
			vocab_size:
			d_hidden:
			embedding_weight: tensor
				if using space:
				```
				nlp = spacy.load('en_vectors_web_lg')
				embed_weights = torch.FloatTensor(nlp.vocab.vectors.data)
				```
		"""
        
        super(BetaModel, self).__init__()
        
        # Get model parameters
        self.n_classes = kwargs.get('n_class', 3)
        dropout = kwargs.get('dropout', 0.)
        d_context = kwargs.get('d_context', -1)
        d_fc_attention = kwargs.get('d_fc_attention', -1)
        self.bidirectional = kwargs.get('bidirectional', False)
        n_lstm = kwargs.get('n_lstm', 1)
        num_heads = kwargs.get('num_heads', 1)
        activation = kwargs.get('activation', 'relu')
        
        # LSTM block
        d_in_lstm = 300
        d_out_lstm = d_context if d_context > 0 else d_in_lstm
        self.lstm = nn.LSTM(input_size=d_in_lstm, hidden_size=d_out_lstm, num_layers=n_lstm, batch_first=True,
                            bidirectional=self.bidirectional, dropout=(n_lstm > 1) * dropout)
        
        if self.bidirectional: d_out_lstm *= 2
        
        self.project_b4_attention = 0 < d_fc_attention < d_out_lstm
        d_attn = d_fc_attention if self.project_b4_attention else d_out_lstm
        
        self.attention = nn.MultiheadAttention(embed_dim=d_out_lstm, num_heads=num_heads, dropout=dropout, kdim=d_attn,
                                               vdim=d_attn)
        
        d_concat = 2 * d_attn
        self.fc_context = FullyConnected(d_concat, d_attn, activation=activation, dropout=dropout)
        
        self.fc_out = nn.Sequential(
            FullyConnected(d_concat, d_attn, activation=activation, dropout=dropout),
            nn.Linear(d_attn, self.n_classes),
            nn.Dropout(p=dropout)
        )
        
        self.softmax = nn.Softmax(dim=1)
    
    def forward_lstm(self, x: torch.LongTensor):
        """
        Forward path at each branch

        Args:
			x: embedding

		Returns:
		"""
        
        # hidden.size() == (1, N, d_out_lstm)
        # hseq.size() == (N, L, d_out_lstm)
        hseq, (hidden, _) = self.lstm(x)

        n_direction = int(self.bidirectional) + 1
        hidden = hidden[-n_direction:].permute(1, 0, 2)  # size() == (N, n_direction, d_out_lstm)
        hidden = hidden.reshape(hidden.size(0), 1, -1)  # size() == (N, 1, n_direction * d_out_lstm)
        
        return hidden, hseq
    
    def forward_contextualize(self, h: torch.tensor, c: torch.tensor):
        """
		Args:
			h: hidden presentation
			c: context vector

		Returns:
		"""
        
        # d_concat = 2 * d_attn
        contextualized = torch.cat((h, c), dim=2).squeeze(dim=0)  # size() == (N, 2 * d_attn)
        contextualized = self.fc_context(contextualized)  # size() == (N, d_attn)
        return contextualized
    
    def forward(self, x: list):
        """

		Args:
			inputs: ( input1 (N, L, h) , input2(N, L, h))

		Returns:

		"""
        # N = batch_size
        # L = sequence_length
        # h = hidden_dim = embedding_size
        # C = n_class
        padding_mask = x[2:]
        x = x[:2]
        
        # Reproduce hidden representation from LSTM
        h_last, h_seq = [torch.empty(0)] * 2, [torch.empty(0)] * 2
        for i in range(2):
            # h_last.size() == (N, 1, d_out_lstm)
            # h_seq.size() == (N, L, d_out_lstm)
            h_last[i], h_seq[i] = self.forward_lstm(x[i])
            
            # Reswapping dimension for multihead attention
            h_last[i] = h_last[i].permute(1, 0, 2)  # size() == (1, N, d_out_lstm)
            h_seq[i] = h_seq[i].permute(1, 0, 2)  # size() == (L, N, d_out_lstm)
        
        # Compute cross attention
        context, attn_weights = [None] * 2, [None] * 2
        for i in range(2):
            # Make padding mask
            #padding_mask = x[i] == padding_vect
            
            # context_1.size() == (N, 1, d_attention)
            # attn_weight_1.size() == (N, 1, L)
            # query=h_last[1-i], key=h_seq[i], value=h_seq[i]
            context[i], attn_weights[i] = self.attention(h_last[1 - i], h_seq[i], h_seq[i], key_padding_mask=padding_mask[i])
        
        # Contextualize in each branch
        contextualized = [torch.empty(0)] * 2
        
        for i in range(2):
            contextualized[i] = self.forward_contextualize(h=h_last[i], c=context[i])
        
        x = torch.cat(tuple(contextualized), dim=1)  # x.size() == (N, d_concat)
        out = self.fc_out(x)  # size() == (N, n_class)
        
        if not self.training:
            out = self.softmax(out)
        
        return out, tuple(attn_weights)

class SpacyLanguageModel(nn.Module):
    
    def __init__(self, spacy_model: str or object):
        super().__init__()
        if isinstance(spacy_model, str):
            spacy_model = spacy.load(spacy_model)
        self.vocab = spacy_model.vocab
    
    def forward(self, x: list):
        assert len(x) > 0, f'Empty vector x'
        assert isinstance(x[0], str), f'x is a vector of string token'
        
        return [torch.tensor([self.vocab[token].vector for token in doc]) for doc in x]
    
    @property
    def shape(self):
        return self.vocab.vectors.shape

class FullyConnected(nn.Module):
    activate_map = {
        'relu': nn.ReLU(inplace=True),
        'tanh': nn.Tanh()
    }

    def __init__(self, d_in: int, d_out: int, dropout: float = 0, activation: str or callable = 'relu'):
        super().__init__()
        if isinstance(activation, str):
            activation = self.activate_map[activation.lower()]
        self.fc = nn.Sequential(
            nn.Linear(d_in, d_out),
            nn.Dropout(p=dropout),
            activation
        )

    def forward(self, x):
        return self.fc(x)


if __name__ == '__main__':
    
    from torch.nn.utils.rnn import pad_sequence
    from data.tokenizer import Tokenizer
    import spacy

    # === Params ===
    spacy_model = spacy.load('fr_core_news_md')
    method = 'general'
    h = spacy_model.vocab.vectors.shape[-1]

    # === Examples ===
    doc1 = [
        'Bonjour tonton',
        'Comment allez-vous?',
        'Nik a les cheveux courts.'
    ]
    doc2 = [
        'On l’utilise principalement entre copains, entre écoliers, entre jeunes…',
        'Ce repas/plat était très bon!',
        'Tina a les cheveux bouclés.'
    ]
    y = [0, 1, 2]
    
    # Tokenize
    # ==============
    tokenizer = Tokenizer(spacy_model=spacy_model, mode=2)
    
    counter = tokenizer.count_tokens(doc1 + doc2)

    vocab = Vocab(counter, specials=['<unk>', '<pad>', '<msk>'])
    tokenizer.vocab = vocab
    # === Test ===
    
    # tokenize
    x1 = [tokenizer.numericalize(d) for d in doc1]
    x2 = [tokenizer.numericalize(d) for d in doc2]
    
    # convert to tensor
    x1 = [torch.tensor(x, dtype=torch.long) for x in x1]
    x2 = [torch.tensor(x, dtype=torch.long) for x in x2]

    x1 = pad_sequence(x1, batch_first=True)
    x2 = pad_sequence(x2, batch_first=True)

    y = torch.tensor(y, dtype=torch.long)

    model = BetaModel(d_in=300, dropout=0,
                                d_fc_lstm=-1, d_fc_attentiion=-1, d_context=-1, n_class=3)

    model.train()

    loss_fn = nn.CrossEntropyLoss()
    optimizer = optim.SGD(model.parameters(), lr=0.001, momentum=0.9)

    for epoch in range(1):
        # reset optimizer
        optimizer.zero_grad()

        preds, _ = model(x1, x2)
        loss = loss_fn(preds, y)

        loss.backward()
        optimizer.step()

        running_loss = loss.item()
        print("[{:0>3d}] loss: {:.3f}".format(epoch + 1, running_loss))

    model.eval()
    predict, _ = model(x1, x2)
    predict = predict.detach()
    print('Prediction:')
    print(predict)
