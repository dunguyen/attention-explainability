import os
import pickle
import shutil
import sys
from collections import Counter
from glob import glob
from itertools import product
import pandas as pd
from torch.utils.data import Dataset
import torch
from torchtext.vocab import Vocab
from os import path
from tqdm import tqdm
from data.abstracts.dataset import PytorchDataset
from data.frnewslink import load_article, format_assgn_line
from data.tokenizer import Tokenizer
from helpers import env


class VectorizingFNLDataset(Dataset):
	
	def __init__(self, rootdir: str, labels: pd.DataFrame, tokenizer: Tokenizer, encoding: str = "ISO-8859-1"):
		'Initialization'
		self.root = rootdir
		self.encoding = encoding
		self.datasets = labels
		self.tokenizer = tokenizer
	
	def __len__(self):
		'Denotes the total number of samples'
		return len(self.datasets)
	
	def vectorize(self, id_article: int):
		seq = load_article(self.root, int(id_article), encoding=self.encoding)
		seq = self.tokenizer.vectorize(seq)
		return torch.tensor(seq)
	
	def __getitem__(self, index):
		'Generates one sample of data'
		# Load data and get label
		N = len(self)
		if index > N:
			index = index % N
		
		row = self.datasets.iloc[index]
		x1 = self.vectorize(row['article1'])
		x2 = self.vectorize(row['article2'])
		y = row['target']
		
		return x1, x2, y


class FrNewsLinkDataset(PytorchDataset):
	
	def __init__(self,
				 data_path: str,
				 tokenizer: Tokenizer,
				 nb_data_max: int = -1,
				 cache_path: str = '_out',
				 cache_force: bool = False,
				 vocab:Vocab=None):
		
		self.data_path = data_path
		self.tokenizer = tokenizer
		self.cache_path = cache_path
		self.cache_dataset_path = path.join(cache_path, 'dataset', 'frnewslink')
		os.makedirs(self.cache_path, exist_ok=True)
		
		self.annot_path = path.join(self.cache_dataset_path, 'ANNOT')
		self.press_path = path.join(self.cache_dataset_path, 'PRESS')
		self.link_path = path.join(self.cache_dataset_path, 'links.csv')
		self.encoding = 'ISO-8859-1'
		
		if not self.is_preprocess():
			self.preprocess()
		
		self.data = self.load_data(cache_force=cache_force)
		if nb_data_max > 0:
			self.data = self.data.sample(frac=1).reset_index(drop=True)
			data_pos = self.data[self.data['target'] > 0][:nb_data_max // 2]
			data_neg = self.data[self.data['target'] < 1][:nb_data_max // 2]
			self.data = pd.concat([data_pos, data_neg]).sample(frac=1).reset_index(drop=True)
		
		self.vocab = vocab
	
	def __len__(self):
		'Denotes the total number of samples'
		return len(self.data)
	
	def __getitem__(self, index):
		'Generates one sample of data'
		# Load data and get label
		N = len(self)
		if index > N:
			index = index % N
		
		# Note: do not use iloc due to incoherent dtype row = self.data.iloc[index]
		x1 = self.numericalize(self.data.loc[index, 'article1'])
		x2 = self.numericalize(self.data.loc[index, 'article2'])
		y = self.data.loc[index, 'target']
		
		return x1, x2, y
	
	def numericalize(self, id_article: int):
		seq = load_article(press_path=self.press_path, id_article=id_article, encoding=self.encoding)
		seq = self.tokenizer.numericalize(seq, self.vocab)
		seq = torch.LongTensor(seq)
		return seq
	
	@staticmethod
	def get_vocab_path(cache_path: str):
		return path.join(cache_path, 'dataset', 'frnewslink', 'vocab.pkl')
	
	@staticmethod
	def has_vocab(cache_path: str = '_out'):
		vocab_path = FrNewsLinkDataset.get_vocab_path(cache_path)
		return path.exists(vocab_path)
	
	@staticmethod
	def load_vocab(cache_path: str = '_out'):
		vocab_path = FrNewsLinkDataset.get_vocab_path(cache_path)
		vocab = super(FrNewsLinkDataset, FrNewsLinkDataset).load_vocab(vocab_path)
		return vocab
	
	def build_vocab(self, cache_path: str = None):
		cache_path = self.cache_path if cache_path is None else cache_path
		vocab_path = FrNewsLinkDataset.get_vocab_path(cache_path)
		
		counter = Counter()
		unvisit = dict()
		for id_article in tqdm(pd.concat([self.data['article1'], self.data['article2']]),
							   desc='Constructing vocab', unit=' label', file=sys.stdout, disable=env.disable_tqdm):
			if unvisit.get(id_article, True):
				text = load_article(self.press_path, id_article, encoding=self.encoding)
				tokens = self.tokenizer(text)
				counter.update(tokens)
				unvisit[id_article] = False
		
		# Buld vocab is painful, so we have to save and load for each time
		vocab = Vocab(counter, specials=['<unk>', '<pad>', '<msk>'])
		with open(vocab_path, 'wb') as f:
			pickle.dump(vocab, f)
			
		if env.disable_tqdm: print('Build vocab at', vocab_path)
		self.vocab = vocab
		return vocab
	
	def load_data(self, cache_force: bool = False) -> pd.DataFrame:
		"""
		Prepare file links.csv that indicate the article id association
		"""
		
		coltype = {'article1': int, 'article2': int, 'target': float}
		
		# If not force to cache, then search existing csv_annot_path
		if not cache_force and path.exists(self.link_path):
			with tqdm(total=1, desc='Load cache', unit='file', file=sys.stdout, disable=env.disable_tqdm) as pbar:
				dataset = pd.read_csv(self.link_path, sep=';').astype(coltype)
				pbar.update(1)
				pbar.set_postfix({'from': self.link_path,
								  'neg': sum(dataset['target'] < 1),
								  'pos': sum(dataset['target'] > 0)})
				
			if env.disable_tqdm: print('Load cache', self.link_path)
			return dataset
		
		# Load all annotation files
		files = glob(path.join(self.annot_path, '*.assgn'))
		assert len(files) > 0, f'Current annot_path({self.annot_path}) does not have any .assgn file.'
		
		# Output
		label_list = [list(), list()]
		pbar = tqdm(files, desc=f'Load assgn files', unit='files', file=sys.stdout)
		for f in pbar:
			with open(f, 'r') as fread:
				for line in fread:
					# Get assgn lines
					beg_seg, end_seg, id_yes, id_maybe, id_no = format_assgn_line(line)
					
					# Remove all of the non existing text
					id_yes = [id for id in id_yes if path.exists(path.join(self.press_path, str(id) + '.txt'))]
					id_no = [id for id in id_no if path.exists(path.join(self.press_path, str(id) + '.txt'))]
					# id_maybe = [id for id in id_maybe if path.exists(path.join(press_path, str(id) + '.txt'))]
					
					# make couple IDs and label
					label_list[1] += [[x, y, 1] for x, y in product(id_yes, id_yes) if x != y]  # positive links
					label_list[0] += [[x, y, 0] for x, y in product(id_yes, id_no) if x != y]  # negative links
		
		data = label_list[0] + label_list[1]
		
		dataset = pd.DataFrame(data, columns=list(coltype.keys()))
		dataset = dataset.astype(coltype)
		
		# Save for next loading time
		dataset.to_csv(self.link_path, index=False, sep=';')
		pbar.set_postfix({'save_path': self.link_path, 'neg': len(label_list[0]), 'pos': len(label_list[1])})
		pbar.close()
		
		return dataset
	
	def preprocess(self):
		
		press_files = []
		annot_files = []
		
		# copy out all press files
		for w0x_xx in os.listdir(self.data_path):
			
			if not path.isdir(path.join(self.data_path, w0x_xx)): continue
			
			press_dir = path.join(self.data_path, w0x_xx, 'PRESS_' + w0x_xx)
			annot_dir = path.join(self.data_path, w0x_xx, 'ANNOT_' + w0x_xx)
			
			press_files += glob(path.join(press_dir, '**', '*.txt'))
			annot_files += glob(path.join(annot_dir, '*.assgn'))
		
		os.makedirs(self.press_path, exist_ok=True)
		for f in tqdm(press_files, desc='Copy press to ' + self.press_path, file=sys.stdout, disable=env.disable_tqdm):
			shutil.copy(f, self.press_path)
		if env.disable_tqdm: print('Copy press to', self.press_path)
		
		os.makedirs(self.annot_path, exist_ok=True)
		for f in tqdm(annot_files, desc='Copy annot to ' + self.annot_path, file=sys.stdout, disable=env.disable_tqdm):
			shutil.copy(f, self.annot_path)
		print('Copy anot to', self.annot_path)
	
	def is_preprocess(self):
		return path.exists(self.press_path) and path.exists(self.annot_path)


class VectorizeFrNewsLinkDataset(FrNewsLinkDataset):
	
	def vectorize(self, id_article: int):
		seq = load_article(self.press_path, int(id_article), encoding=self.encoding)
		seq = self.tokenizer.vectorize(seq)
		return torch.tensor(seq)
	
	def __getitem__(self, index):
		'Generates one sample of data'
		# Load data and get label
		N = len(self)
		if index > N:
			index = index % N
		x1 = self.vectorize(self.data.loc[index, 'article1'])
		x2 = self.vectorize(self.data.loc[index, 'article2'])
		y = self.data.loc[index, 'target']
		
		return x1, x2, y