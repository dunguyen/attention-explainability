import spacy
import torch
from torch.utils.data import random_split, DataLoader

from data.abstracts.handler import AbstractHandler
from data.collate import PaddingCollate, VectorizingCollate
from data.frnewslink import FrNewsLinkTokenizer
from data.frnewslink.dataset import FrNewsLinkDataset


class FrNewsLinkHandler(AbstractHandler):
	
	def __init__(self, data_path, cache_path, batch_size: int = 32, nb_data_max=-1, verbose=True, cache_force=False):
		super().__init__()
		self.n_class = 1
		self.spacy_model = spacy.load('fr_core_news_md')
		self.tokenizer = FrNewsLinkTokenizer(spacy_model=self.spacy_model)
		self.vocab = None
		
		context_arg = dict(cache_path=cache_path, verbose=verbose)
		
		if not cache_force and FrNewsLinkDataset.has_vocab(cache_path=cache_path):
			vocab = FrNewsLinkDataset.load_vocab(**context_arg)
		else:
			fullset = FrNewsLinkDataset(data_path=data_path, tokenizer=self.tokenizer, **context_arg)
			vocab = fullset.build_vocab(**context_arg)
		
		self.vocab = vocab
		padding_idx = vocab['<pad>']
		dataset = FrNewsLinkDataset(data_path=data_path,
									tokenizer=self.tokenizer,
									nb_data_max=nb_data_max * 3,
									vocab=vocab,
									**context_arg)
		collate_fn = PaddingCollate(multiclass=False, padding_idx=padding_idx)
		
		train_set, val_set, test_set = random_split(dataset, [nb_data_max] * 3, generator=torch.Generator())
		generator_arg = dict(collate_fn=collate_fn, batch_size=batch_size, shuffle=True)
		self.train_generator = DataLoader(train_set, **generator_arg)
		self.val_generator = DataLoader(val_set, **generator_arg)
		self.test_generator = DataLoader(test_set, **generator_arg)


class VectorizeFNLHandler(AbstractHandler):
	
	def __init__(self, data_path, cache_path, batch_size: int = 32, nb_data_max=-1, cache_force=False):
		super().__init__()
		self.n_class = 1
		self.spacy_model = spacy.load('fr_core_news_md')
		self.tokenizer = FrNewsLinkTokenizer(spacy_model=self.spacy_model)
		
		dataset = FrNewsLinkDataset(data_path=data_path, tokenizer=self.tokenizer, cache_path=cache_path,
									cache_force=cache_force)
		
		collate_fn = VectorizingCollate(multiclass=False, batch_first=True)
		
		train_set, val_set, test_set = random_split(dataset, [nb_data_max] * 3, generator=torch.Generator())
		
		generator_arg = dict(collate_fn=collate_fn, batch_size=batch_size, shuffle=True)
		self.train_generator = DataLoader(train_set, **generator_arg)
		self.val_generator = DataLoader(val_set, **generator_arg)
		self.test_generator = DataLoader(test_set, **generator_arg)

