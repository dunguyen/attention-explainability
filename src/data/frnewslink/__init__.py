import os
import pickle
import sys
from collections import Counter

from torchtext.vocab import Vocab

from data.frnewslink.tokenizer import FrNewsLinkTokenizer
from data.processing_data import tokenize, TK_LEMMA
from exception import *
from os import path
from glob import glob
import shutil
from tqdm import tqdm
import numpy as np
from itertools import product
import pandas as pd


def format_assgn_line(assgn_line):
    """ Read each assgn line and cut into 5 entities: (beg_seg, end_seg, id_yes, id_maybe, id_no)

    Args:
        assgn_line: (str) retrieving lines from .assgn file

    Returns:
        beg_seg (float): beginning time of transcript segment
        end_seg (float): ending time of transcript segment
        id_yes (list): id of related articles (prob link = 1.)
        id_maybe (list): id of probably related articles
        id_no (list): id of unrelated articles (prob link = 0.)
    """
    # clean white characters
    assgn_line = assgn_line.rstrip()

    # segment : deb_seg : fin_seg
    # id_yes: list of article id associated with this segment.
    # id_maybe:
    segment, id_yes, id_maybe, id_no = assgn_line.split(';')
    beg_seg, end_seg = segment.split(':')

    id_list = [id_yes, id_maybe, id_no]
    for i in range(3):
        id_list[i] = id_list[i].split(' ')
        try:
            id_list[i] = map(int, id_list[i])
            id_list[i] = list(id_list[i])
        except ValueError:
            # case no int value, or containing other thing than int value
            id_list[i] = []

    # reformat for correct type
    beg_seg = float(beg_seg)
    end_seg = float(end_seg)
    id_yes = id_list[0]
    id_maybe = id_list[1]
    id_no = id_list[2]

    return beg_seg, end_seg, id_yes, id_maybe, id_no

CNT_FILE = 0
CNT_DATROW = 1

#TODO obsolete
def load_labels(annot_path: str, press_path: str=None, nb_data_max: int=-1, split_ratio=None, cache_dir: str = '_out',
                cache_force: bool = False):
    """
    Read all assgn files, cache in file and shuffle data, then take data in balanced way
    Args:
        annot_path: path to annotation file
        nb_data_max (int): maximum number of data to load
        press_path: (optional) path to press file
        cache_dir (str): where to save the temporary file
        cache_force (bool): whether to override the saved file
    Returns:
        array of N x 3, where N is number of data. Each row has form [int, int, int] == [doc1, doc2, label (0,1) ]
    """
    
    if split_ratio is None: split_ratio = [.7, .15]
    cache_dir = path.join(cache_dir, 'labels')
    save_path = path.join(cache_dir, 'frnewslink.csv')
    coltype = {'article1': int, 'article2': int, 'target': float}

    # If not force to cache, then search existing save_path
    if not cache_force and path.exists(save_path):
        
        with tqdm(total=1, desc='Load cache', unit='file', file=sys.stdout) as pbar:
            labels = pd.read_csv(save_path, dtype=coltype, sep=';')
            pbar.update(1)
            pbar.set_postfix({'save_path': save_path, 'neg': sum(labels['target'] < 1), 'pos': sum(labels['target'] > 0)})
    # If save_path not existed, then generate data
    else:
        # Load all annotation files
        files = glob(path.join(annot_path, '*.assgn'))
        assert len(files) > 0, f'Current annot_path({annot_path}) does not have any .assgn file.'

        # Output
        label_list = [list(), list()]
        pbar = tqdm(files, desc=f'Load assgn files', unit='files', file=sys.stdout)
        for f in pbar:
            with open(f, 'r') as fread:
                for line in fread:
                    # Get assgn lines
                    beg_seg, end_seg, id_yes, id_maybe, id_no = format_assgn_line(line)

                    # Remove all of the non existing text
                    if press_path is not None:
                        id_yes = [id for id in id_yes if path.exists(path.join(press_path, str(id) + '.txt'))]
                        id_no = [id for id in id_no if path.exists(path.join(press_path, str(id) + '.txt'))]
                    # id_maybe = [id for id in id_maybe if path.exists(path.join(press_path, str(id) + '.txt'))]

                    # make couple IDs and label
                    line_pos: list = [[x, y, 1] for x, y in product(id_yes, id_yes) if x != y]
                    line_neg: list = [[x, y, 0] for x, y in product(id_yes, id_no) if x != y]

                    # Store them in the list
                    label_list[0] += line_neg
                    label_list[1] += line_pos

        data = label_list[0] + label_list[1]

        labels = pd.DataFrame(data, columns=list(coltype.keys()))

        # Save for next loading time
        os.makedirs(cache_dir, exist_ok=True)
        labels.to_csv(save_path, index=False, sep=';')
        pbar.set_postfix({'save_path': save_path, 'neg': len(label_list[0]), 'pos': len(label_list[1])})
        pbar.close()

    # shuffle data
    labels = labels.sample(frac=1)

    # Select an equivalent number of data rows
    if nb_data_max > 0:
        nb_half = nb_data_max // 2
        neg_data = labels[labels['target'] == 0].head(nb_half)
        pos_data = labels[labels['target'] == 1].head(nb_half)
        labels = neg_data.append(pos_data).sample(frac=1)

    # Split into 3 set for training
    labels.reset_index()
    nb_train = len(labels) * split_ratio[0]
    nb_val = len(labels) * split_ratio[1] if len(split_ratio) > 1 else len(labels) * (1 - split_ratio[0])
    nb_train, nb_val = int(nb_train), int(nb_val)
    train_set = labels.iloc[:nb_train]
    val_set = labels.iloc[nb_train:nb_train + nb_val]
    test_set = labels.iloc[nb_train + nb_val:]
    
    print('Train set: nbpos=', sum(train_set['target'] == 1), '; nbneg=', sum(train_set['target'] == 0), '; #', len(train_set))
    print('Val set: nbpos=', sum(val_set['target'] == 1), '; nbneg=', sum(val_set['target'] == 0), '; #', len(val_set))
    print('Test set: nbpos=', sum(test_set['target'] == 1), '; nbneg=', sum(test_set['target'] == 0), '; #', len(test_set))

    return train_set, val_set, test_set

# TODO obselete and should be update from Dataset
def build_vocab(label_list, press_path, encoding, spacy_model, cache_dir: str = '_out', cache_force: bool = False):
    # Inspired from
    # https://pytorch.org/tutorials/beginner/text_sentiment_ngrams_tutorial.html#prepare-data-processing-pipelines
    counter = Counter()
    unvisit = dict()

    cache_path = path.join(cache_dir, 'labels', 'frnewslink_vocab.pkl')
    if not cache_force and path.exists(cache_path):
        with tqdm(total=1, desc='Load cache vocab', file=sys.stdout) as bar:
            with open(cache_path, 'rb') as f:
                vocab = pickle.load(f)
            bar.update(1)
            bar.set_postfix({'path': cache_path})
        return vocab

    for doc1, doc2, _ in tqdm(label_list, desc='Constructing vocab', unit=' label', file=sys.stdout):
        if unvisit.get(doc1, True):
            seq = load_article(press_path, doc1, encoding=encoding)
            tokens = tokenize(seq, TK_LEMMA, spacy_model)
            counter.update(tokens)
            unvisit[doc1] = False

        if unvisit.get(doc2, True):
            seq = load_article(press_path, doc2, encoding=encoding)
            tokens = tokenize(seq, TK_LEMMA, spacy_model)
            counter.update(tokens)
            unvisit[doc2] = False

    # Buld vocab is painful, so we have to save and load for each time
    vocab = Vocab(counter, specials=['<unk>', '<pad>', '<msk>'])
    with open(cache_path, 'wb') as f:
        pickle.dump(vocab, f)
    return vocab


def _format_content(fpath: str, encoding: str = 'ISO-8859-1') -> dict:
    
    with open(fpath, encoding=encoding) as f:
        content = f.read()

        tokens = ['TITRE:', 'DATE:', 'URL:', 'PRINCIPAL:', 'TEXT:']

        data = {}
        for begin, end in zip(tokens[:-1], tokens[1:]):
            data[begin[:-1]] = content[content.index(begin) + len(begin): content.index(end)]

        data[tokens[-1][:-1]] = content[content.index(tokens[-1]) + len(tokens[-1]):]
        return data


def load_article(press_path: str, id_article: int, encoding: str = 'ISO-8859-1'):
    article_path = path.join(press_path, f"{id_article}.txt")
    content = _format_content(article_path, encoding)['TEXT']
    content = content.strip('\n').strip(' ')
    return content