from spacy.tokens import Token

from data.tokenizer import Tokenizer


class FrNewsLinkTokenizer(Tokenizer):

    def __init__(self, spacy_model):
        super().__init__(spacy_model, filter_fn=self.filter)

    def filter(self, token: Token):
        """
        Set true to ignore this word during constructing vector
        Args:
            token:

        Returns:

        """
        lemma_norm = token.vocab[token.lemma_].norm
        return self.mask(token) or (lemma_norm not in token.vocab.vectors.key2row)

    def mask(self, token: Token):
        """
        Set true if we want to mask the token away from attention.
        Args:
            token:

        Returns:

        """
        mask_pos = ['ADP', 'SYM', 'PUNCT', 'DET']
        return token.is_space or (token.pos_ in mask_pos)


class SpacyEmbFNLTokenizer(FrNewsLinkTokenizer):
    def __init__(self, spacy_model):
        super().__init__(spacy_model)
        
    def numericalize(self, text, vocab) -> list:
        tokens = []
        for tk in self.tokenize(text):
            if self.filter_fn(tk):
                lemma_norm = tk.vocab[tk.lemma_].norm
                try:
                    id = tk.vocab.vectors.key2row[lemma_norm]
                    tokens.append(id)
                except KeyError:
                    # print('tk = ', tk, ' lemma = ', tk.lemma_, lemma_norm in tk.vocab.vectors.key2row)
                    pass
        return tokens