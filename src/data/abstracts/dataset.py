import pickle
import sys
from abc import ABC
from os import path

from torch.utils.data import Dataset
from tqdm import tqdm

from helpers import env


class PytorchDataset(Dataset, ABC):

    @staticmethod
    def load_vocab(cache_path: str='_out'):
        vocab_path = cache_path if '.pkl' in cache_path else path.join(cache_path, 'vocab.pkl')
        with tqdm(total=1, desc='Load cache vocab', file=sys.stdout, disable=env.disable_tqdm) as bar:
            with open(vocab_path, 'rb') as f:
                vocab = pickle.load(f)
            bar.update(1)
            bar.set_postfix({'path': vocab_path})
            
        if env.disable_tqdm: print('Load cache vocab from', cache_path)
        
        return vocab
