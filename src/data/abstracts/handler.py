from abc import ABC
from collections import Counter

from torch.utils.data import DataLoader, Dataset
from torchtext.vocab import Vocab


class AbstractHandler(ABC):
	
	def __init__(self):
		self.vocab = Vocab(Counter())
		self.train_generator = DataLoader(Dataset(), batch_size=32)
		self.val_generator = DataLoader(Dataset(), batch_size=32)
		self.test_generator = DataLoader(Dataset(), batch_size=32)
		
		