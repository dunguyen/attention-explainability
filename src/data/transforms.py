import string
from os import path
from typing import Union

import torch
from torch.nn import Module
from torch.nn.utils.rnn import pad_sequence
from torchtext.vocab import build_vocab_from_iterator, Vocab
from torchtext.vocab.vectors import pretrained_aliases as pretrained, Vectors

from helpers.logger import log


class SpacyTokenizerTransform(Module):
	"""
	Turns texts into Spacy tokens
	"""

	def __init__(self, spacy_model):
		super(SpacyTokenizerTransform, self).__init__()
		self.sm = spacy_model
		
	def forward(self, texts):
		if isinstance(texts, str):
			return [tk.text for tk in self.sm(texts.strip())]
		docs = self.sm.pipe(texts)
		return [[tk.text for tk in doc] for doc in docs]
	
	def __str__(self):
		return 'spacy'

class LemmaLowerTokenizerTransform(SpacyTokenizerTransform):
	"""
	Transforms list of sentence into list of word array. Words are lemmatized and lower cased
	"""
	def forward(self, texts: Union[str, list]):
		
		if isinstance(texts, str):
			return [tk.lemma_.lower() for tk in self.sm(texts.strip())]
		
		texts = [t.strip() for t in texts]
		return [[tk.lemma_.lower() for tk in doc] for doc in self.sm.pipe(texts)]
	
	def __str__(self):
		return 'lemma-lower'

class GoldLabelTransform(Module):
	"""
	Turn eSNLI gold label into corresponding class id
	"""
	def __init__(self, label_map: dict = None):
		super(GoldLabelTransform, self).__init__()
		self.lmap_ = label_map if label_map is not None else {'neutral': 0, 'entailment': 1, 'contradiction': 2}
		
	def forward(self, labels: Union[list, str]):
		if isinstance(labels, str):
			return self.lmap_[labels]
		return [self.lmap_[l] for l in labels]


class HighlightTransform(Module):
	"""
	Turn human highlights into boolean mask, True if it's attention
	"""
	
	def forward(self, highlight: Union[list, str]):
		masks = [list()] * len(highlight)
		for idx, phrase in enumerate(highlight):
			mask = []
			is_highlight = False
			for token in phrase:
				if token == '*':
					is_highlight = not is_highlight
					continue
				mask.append(is_highlight and token not in string.punctuation)
			masks[idx] = mask
		return masks

	
class HeuristicTransform(Module):
	
	def __init__(self, vectors:str or Vectors, spacy_model, cache=None, normalize:str=None):
		super(HeuristicTransform, self).__init__()
		
		self.N_SOFTMAX = 'softmax'
		self.N_LOG_SOFTMAX = 'log_softmax'
		NORMS = [self.N_SOFTMAX, self.N_LOG_SOFTMAX]
		
		if normalize is not None:
			normalize = normalize.lower()
			assert normalize in NORMS, f'Undefined normalization: {normalize}. Possible values: {NORMS}'
		self.normalize = normalize
		self.sm = spacy_model
		if isinstance(vectors, str):
			vectors = pretrained[vectors](cache=cache)
		self.vectors = vectors
		self.POS_FILTER = ['VERB', 'NOUN', 'ADJ']
		self.INF = 1e30
		self.EPS = 1e-20
		
	def forward(self, premise, hypothesis):
		batch = {'premise': premise, 'hypothesis': hypothesis}
		vectors = {}
		mask = {}
		heuristic = {}

		for side, texts in batch.items():
			docs = list(self.sm.pipe(texts))
			
			# POS-tag mask: True on informative tokens
			# pos = [[tk.pos_ for tk in d] for d in docs]
			pos_mask = [torch.tensor([(not tk.is_stop) and (tk.pos_ in self.POS_FILTER) for tk in d]) for d in docs]
			
			for idx in range(len(pos_mask)):
				if (~pos_mask[idx]).all():
					pos_mask[idx] = torch.tensor([not tk.is_stop for tk in docs[idx]])
				if (~pos_mask[idx]).all():
					pos_mask[idx] = torch.tensor([True for _ in docs[idx]]) # Uniform attention
			
			pos_mask = pad_sequence(pos_mask, batch_first=True, padding_value=False)
			
			tokens = [[tk.lemma_.lower() for tk in d] for d in docs]
			v = [self.vectors.get_vecs_by_tokens(tk) for tk in tokens]
			v = pad_sequence(v, batch_first=True)
			v_norm = v / (v.norm(dim=2)[:, :, None] + self.EPS)
			
			vectors[side] = v_norm
			mask[side] = pos_mask
		
		# similarity matrix
		similarity = torch.bmm(vectors['premise'], vectors['hypothesis'].transpose(1, 2))
		
		# apply mask
		for side, dim in zip(batch.keys(), [2, 1]):
			heuristic[side] = similarity.sum(dim).masked_fill_(~mask[side], - self.INF)
			
		# Normalize heuristic
		if self.normalize == self.N_SOFTMAX:
			heuristic = {k: h.softmax(1) for k, h in heuristic.items()}
		elif self.normalize == self.N_LOG_SOFTMAX:
			heuristic = {k: h.log_softmax(1) for k, h in heuristic.items()}
			
		return heuristic


class PaddingToken(Module):
	
	def __init__(self, pad_value):
		super(PaddingToken, self).__init__()
		self.pad_value = pad_value
	
	def forward(self, text):
		txt_lens = [len(t) for t in text]
		max_len = max(txt_lens)
		txt_lens = [max_len - l for l in txt_lens]
		for i in range(len(text)):
			text[i] += [self.pad_value] * txt_lens[i]
		return text
	

if __name__ == '__main__':
	# Unit test
	
	import spacy
	import torchtext.transforms as T
	from data.esnli.dataset import ESNLIDataPipe
	
	cache_path = path.join('/Users', 'dunguyen', 'Projects', 'nlp', 'src', '_out', 'dataset')
	sm = spacy.load('en_core_web_md')
	text_transform = T.Sequential(
        SpacyTokenizerTransform(sm)
	)

	dp = ESNLIDataPipe(root=cache_path, split='test')
	
	def list2dict(batch):
		# handle case where no batch
		if isinstance(batch, dict):
			return {k: list(v) for k, v in batch.items()}
		# in case of batch
		return {k: [row[k] for row in batch] for k in batch[0]}
	
	def batch_transform(batch): return text_transform(batch['premise'] + batch['hypothesis'])
	dp = dp.batch(8).map(list2dict).flatmap(batch_transform)
	
	print('build vocab')
	vocab = build_vocab_from_iterator(iter(dp))
	print('Done build vocab')
	print(vocab)
	print(vocab.get_stoi())
