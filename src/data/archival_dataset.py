import json
import logging
import os
import pickle
import shutil
import sys
from collections import Counter

import pandas as pd
from torch.utils.data import Dataset
from os import path

from torchtext.utils import download_from_url, extract_archive
from torchtext.vocab import Vocab
from tqdm import tqdm

from data.pipeline import TextPipeline
from exception import InvalidOperationError
from helpers import env


class ArchivalDataset(Dataset):
    LABEL = ['neutral', 'entailment']
    PATH = 'D:/stage/scriptarchival/archival_dataset/'
    SPLITS = ['train', 'test', 'val']

    def __init__(self, split: str = 'train', cache_path: str = '_out', n: int = -1, shuffle=True):
        """

        Args:
            split       (str):
            cache_path  (str):
            n           (int): max of data to be loaded
            shuffle     (bool): shuffle if load limited data
                        If n is precised and shuffle = True, dataset will sample n datas.
                        If n is precised and shuffle = False, dataset will take only n first datas.
        """

        # assert
        if split not in self.SPLITS:
            raise InvalidOperationError(f'split {split} doesnt exist in Archival')

        self.split = split
        self.classes = self.LABEL
        root = self._root(cache_path=cache_path)
        self.csv_path = path.join(root, f'archival_dataset_{split}.csv')
        self.zip_path = path.join(root, 'archival_dataset.zip')

        # Load csv into dataset
        # if path.exists(self.csv_path):
        self._full = self._csv2data()

        # translate jsonl into csv if not exist
        """else:

            # unzip into csv file if not exist
            self._full = self._zip_to_csv(extract_path=root)"""

        self._full = self._full.drop(columns='Unnamed: 0')
        blanks = list(self._full.loc[self._full['premise'].str.len() < 10].index) + list(self._full.loc[self._full['hypothesis'].str.len() < 10].index)
        if len(blanks) > 0:
            self._full.drop(blanks, inplace=True)
            self._full.reset_index(drop=True, inplace=True)

        self.data = self._full
        self.classes = self.LABEL
        self._encode_label()

        if n > 0:
            n = n // 2
            if shuffle:
                self.data = self.data.sample(frac=1).reset_index(drop=True)
            subset = [pd.DataFrame()] * 2
            for label in range(2):
                subset[label] = self.data[self.data['class'] == label]
                subset[label] = subset[label].sample(n=n) if shuffle else subset[label].head(n)
            self.data = pd.concat(subset).reset_index(drop=True)

    def __getitem__(self, index: int):
        """

        Args:
            index ():

        Returns:

        """

        # Load data and get label
        if index >= len(self): raise IndexError

        doc1 = self.data['premise'][index]
        doc2 = self.data['hypothesis'][index]
        y = self.data['class'][index]

        return (doc1, doc2), y

    def __len__(self):
        """
        Denotes the total number of samples
        Returns: int
        """
        return len(self.data)

    def __str__(self):
        return self.data.describe()

    def select_class(self, filters: list, reset_id: bool = False):
        """
        Select class to keep, used in case to binarize the dataset

        Args:
            filters     (list): list of keeping class
                        List of keeping class, as label name (ex. ['neutral']) or its id (ex. [1, 0])
            reset_id    (bool): reset class id, only do when given string in filter
                        When true, reset class id from 0. (Ex: filters=['neutral', 'contradiction']
                        => {neutral: 0, contradiction: 1})

        Returns:
            new class label (dict)
        """

        assert 0 < len(filters) < 3, f'{filters} must constrain 1 or 2 class'

        # Case keep the original classes
        if len(filters) == 2:
            self.classes = self.LABEL
            self.data = self._full
            return self.classes

        # Case filter some class:
        if type(filters[0]) == int:
            filters = [self.LABEL[l] for l in filters]  # [0, 1, 2] -> ['neutral', 'entail', 'contracdict']

        # Filter out category
        self.data = self._full[self._full['judgment'].isin(filters)].copy()
        self.classes = filters

        # If given string and reset idx, then reset class encoding
        if type(filters[0]) == str and reset_id:
            self._encode_label()

        return self

    @classmethod
    def _root(self, cache_path: str):
        return path.join(cache_path, 'dataset', 'archival')

    def _csv2data(self, csv_path=None, split=None):
        """
        load csv into data
        """
        if csv_path is None: csv_path = self.csv_path
        if split is None: split = self.split

        if not path.exists(csv_path):
            raise InvalidOperationError(f'File csv {csv_path} does not exist')

        coltype = {'premise': str, 'hypothesis': str, 'label': 'category'}
        desc = f'Load {split}'

        with tqdm(total=1, desc=desc, file=sys.stdout, disable=env.disable_tqdm) as bar:
            dataset = pd.read_csv(csv_path, dtype=coltype)

            if dataset.isnull().values.any():
                dataset = dataset.dropna().reset_index()
                dataset.to_csv(csv_path, index=False)

            dataset.drop(['premiseid', 'hypothesisid'], axis=1, inplace=True)
            bar.update(1)
            bar.set_postfix({'path': csv_path})

        if env.disable_tqdm: print(desc, ', path:', csv_path)
        return dataset


    def _encode_label(self):
        self.data['judgment'] = self.data['judgment'].astype('category')
        self.data['judgment'] = self.data['judgment'].cat.rename_categories(['neutral', 'entailment'])
        self.data['judgment'] = self.data['judgment'].cat.reorder_categories(self.classes)
        self.data['class'] = self.data['judgment'].cat.codes

    def _zip_to_csv(self, extract_path):
        files = extract_archive(self.zip_path, extract_path)
        return files


def load_dataset(cache_path: str = '_out', n: int = -1) -> dict:
    return {
        'train': ArchivalDataset('train', cache_path, n, shuffle=True),
        'val': ArchivalDataset('dev', cache_path, n),
        'test': ArchivalDataset('test', cache_path, n)
    }


def build_vocab(dataset: ArchivalDataset, vectors: str = None, cache_path: str = '_out',
                pipeline: TextPipeline = None) -> Vocab:
    vocab_path = path.join(cache_path, 'dataset', 'archival', f'vocab_{pipeline}.pkl')
    vector_path = path.join(cache_path, 'model', '.vector_cache', vectors)

    if path.exists(vocab_path):
        with tqdm(total=1, desc=f'Load vocab {vocab_path}', file=sys.stdout, disable=env.disable_tqdm) as bar:
            with open(vocab_path, 'rb') as f:
                vocab = pickle.load(f)
            bar.update(1)

        if env.disable_tqdm: logging.info(f'Load vocab from {cache_path}')

        return vocab

    # Make new vocab and save to cache
    sentences = pd.concat([dataset.data['premise'], dataset.data['hypothesis']])
    counter = Counter()
    for tokens in tqdm(pipeline(sentences), desc=f'{vocab_path}', unit='sentences', file=sys.stdout,
                       disable=env.disable_tqdm):
        counter.update(tokens)

    if vectors is not None:
        vocab = Vocab(counter, min_freq=1, vectors=vectors, vectors_cache=vector_path)
    else:
        vocab = Vocab(counter, min_freq=1)

    # Save for next time use
    with open(vocab_path, 'wb') as f:
        pickle.dump(vocab, f)
        logging.info('Building vocab at', vocab_path, 'vector_cache:', vector_path)

    return vocab
