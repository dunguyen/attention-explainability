import torch
from torch.nn.utils.rnn import pad_sequence

from data.pipeline import *
from helpers.tools import extract_highlight, check_length


class StandardCollate:
	"""
	StandardCollate would transform raw text data into a numeric token using the pipeline given in argument. Then each
	line would be added some more padding given in `padding`.

	Args:
		padding (int): padding id to fill
		pipeline (NumericalizePipeline): the pipeline that define how to process text
		multiclass (bool): If true define y as long (id of class). If false define y as float (probability). Default: True
		batch_first (bool): Whether to make the first dimension to be the batch size. Default: True
		device (): pytorch device to send data

	Example:
		data_batch =    [['They are ready to run.', 'Kid is swimming.'],
						 ['A man is asleep.','A woman is sitting.']]

		<br/>(2, bsz) --> (2, bsz, nb_token)<br/>
		[[['They', 'are', 'ready', 'to', 'run', '.'], ['Kid', 'is', 'swimming', '.']],
		 [['A', 'man', 'is', 'asleep', '.'], ['A', 'woman', 'is', 'sitting', '.']]]

		<br/>(2, bsz, nb_token) --> (2, bsz, nb_token)<br/>
		[[518, 9, 1747, 127, 2437, 3515], [20, 707, 7, 6]],
		 [[159, 7, 550, 42, 16], [14, 24, 16, 358, 19]]]

		<br/>(2, bsz, nb_token) --> (2, bsz, padding_length)<br/>
		[[518, 9, 1747, 127, 2437, 3515],
		 [20, 707, 7, 6, 0, 0]],
		[[159, 7, 550, 42, 16],
		 [14, 24, 16, 358, 19]
		]]
	"""
	
	def __init__(self, padding, pipeline: NumericalizePipeline, multiclass: bool = True, batch_first: bool = True,
	             device=None):
		assert isinstance(pipeline, NumericalizePipeline), f'Please provide the corresponding type of pipeline'
		
		self._device = device if device else torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
		self._pipeline = pipeline
		self._multiclass = multiclass
		self._batch_first = batch_first
		self._padding_idx = padding
		self.sent_length = list()
	
	def __call__(self, batch):
		"""
		Transform a raw batch of text into vectors
		Args:
			batch (list): list of text. Shape of list (nb_line, 2)

		Returns:
			x (tuple): containing [premise, hypothesis], each of which is an array of numeric token.
			y (torch.tensor)

		"""
		x, y = zip(*batch)
		
		x = list(map(list, zip(*x)))  # flip x, ie. permute(1,0) dimension
		# x is now of shape [2, nb line, sent length]
		self.sent_length = [list()] * len(x)
		
		for i in range(len(x)):
			# numericalize tokens
			x[i] = self._pipeline(x[i])
			# save the original length (before padding)
			self.sent_length[i] = [len(_x) for _x in x[i]]
			# turn into tensor
			x[i] = [torch.tensor(_vector).to(self._device, non_blocking=True) for _vector in x[i]]
			# add padding
			x[i] = pad_sequence(x[i], padding_value=self._padding_idx, batch_first=self._batch_first)
		
		y = torch.LongTensor(y) if self._multiclass else torch.FloatTensor(y)
		
		return tuple(x), y.to(self._device)


class MaskingCollate:
	
	def __init__(self, padding, num_pipeline: NumericalizePipeline, mask_pipeline: MaskPipeline, multiclass: bool = True, batch_first: bool = True,
	             device=None):
		assert isinstance(num_pipeline, NumericalizePipeline), f'Please provide the corresponding type of pipeline'
		
		self._device = device if device else torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
		self._num_pipeline = num_pipeline
		self._mask_pipeline = mask_pipeline
		self._multiclass = multiclass
		self._batch_first = batch_first
		self._padding_idx = padding
		self.sent_length = list()
	
	def __call__(self, batch):
		"""
		Transform a raw batch of text into vectors
		Args:
			batch (list): list of text. Shape of list (nb_line, 2)

		Returns:
			x (tuple): containing [premise, hypothesis, mask_premise, mask_hypothesis], each of which is an array of numeric token.
			y (torch.tensor)

		"""
		x, y = zip(*batch)
		
		x = list(map(list, zip(*x)))  # flip x, ie. permute(1,0) dimension
		tokens = [None, None]
		mask = [None, None] # init map
		# x is now of shape [2, nb line, sent length]
		self.sent_length = [list()] * len(x)
		
		# i == 0 for premise, 1 for hypothesis
		for i in range(len(x)):
			# numericalize tokens
			tokens[i] = self._num_pipeline(x[i])
			# turn into tensor
			tokens[i] = [torch.tensor(_vector).to(self._device, non_blocking=True) for _vector in tokens[i]]
			# build mask
			mask[i] = self._mask_pipeline(x[i])
			mask[i] = [torch.tensor(_mask_vector, dtype=bool).to(self._device, non_blocking=True) for _mask_vector in mask[i]]
			# add padding
			tokens[i] = pad_sequence(tokens[i], padding_value=self._padding_idx, batch_first=self._batch_first)
			mask[i] = pad_sequence(mask[i], padding_value=True, batch_first=self._batch_first)
		
		y = torch.LongTensor(y) if self._multiclass else torch.FloatTensor(y)
		
		return tuple(tokens + mask), y.to(self._device)


class SpacyLMCollate:
	"""
	SpacyLMCollate would transform raw text data into vectors directly using spacy model. An additional masking array
	is given in the return.

	Args:
		padding (int): padding id to fill
		pipeline (NumericalizePipeline): the pipeline that define how to process text
		multiclass (bool): If true define y as long (id of class). If false define y as float (probability). Default: True
		batch_first (bool): Whether to make the first dimension to be the batch size. Default: True
		device (): pytorch device to send data

	Returns: A list of size 2: premise and hypothesis, each of which contains 2 arrays: 1 containing vectors and the
	other the masking vector
	"""
	
	def __init__(self, padding, spacy_model, multiclass: bool = True, batch_first: bool = True, device=None):
		self._device = device if device else torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
		self._spacy_model = spacy_model
		self._multiclass = multiclass
		self._batch_first = batch_first
		self.padding = padding
	
	def __call__(self, batch):
		"""
		Transform a raw batch of text into vectors
		Args:
			batch (list): list of text. Shape of list (nb_line, 2)

		Returns:
			x (tuple): containing [premise, hypothesis], each of which containing an array of vector and padding mask
			y (torch.tensor)

		"""
		x, y = zip(*batch)
		
		x = list(map(list, zip(*x)))  # flip x == permute(1,0) dimension
		padding_mask = [list(), list()]
		for i in range(len(x)):
			# process texts, x contains Doc spacy
			x[i] = self._pipeline(x[i])
			# turns docs into vectors
			x[i] = [torch.tensor(_vector).to(self._device, non_blocking=True) for _vector in x[i]]
			# make mask along vectors
			padding_mask[i] = [torch.tensor([False] * len(_x)).to(self._device, non_blocking=True) for _x in x[i]]
			# padding vectors, also padding the masks
			x[i] = pad_sequence(x[i], padding_value=0., batch_first=self._batch_first)
			padding_mask[i] = pad_sequence(padding_mask[i], padding_value=1., batch_first=self._batch_first)
		
			padding_mask[i] = pad_sequence(padding_mask[i], padding_value=True, batch_first=self._batch_first)
			
		# fusion token vectors and mask
		x = tuple(x + padding_mask)
		y = torch.LongTensor(y) if self._multiclass else torch.FloatTensor(y)
		
		return tuple(x), y.to(self._device)


class ExplanationESNLICollate(StandardCollate):
	
	def __init__(self, padding, pipeline: NumericalizePipeline, tokenizer_pipeline, multiclass: bool = True, batch_first: bool = True,
	             device=None):
		assert isinstance(pipeline, NumericalizePipeline), f'Please provide the corresponding type of pipeline'
		
		self._device = device if device else torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
		self._pipeline = pipeline
		self._tokenizer_pipeline = tokenizer_pipeline
		self._multiclass = multiclass
		self._batch_first = batch_first
		self._padding_idx = padding
		self.sent_length = list()
	
	def __call__(self, batch):
		"""
		Transform a raw batch of text into vectors
		Args:
			batch (list): list of text. Shape of list (nb_line, 2)

		Returns:
			x (tuple): containing [premise, hypothesis], each of which is an array of numeric token.
			y (torch.tensor)

		"""
		x, y, highlights = zip(*batch)
		
		# Let the standard collate numerize the x and y
		vector_x, vector_y = super(ExplanationESNLICollate, self).__call__([(x_line, y_line) for x_line, y_line in zip(x, y)])
		highlights = list(map(list, zip(*highlights))) # transpose highlight from (Nx2) into (2xN)
		highlights_map = [list()]*2
		
		# Now convert highlights into boolean:
		for i in range(len(highlights)):
		
			hl = extract_highlight(highlights[i], self._tokenizer_pipeline)
			# turn into tensor
			hl = [torch.tensor(_vector).to(self._device, non_blocking=True) for _vector in hl]
			# add padding
			hl = pad_sequence(hl, padding_value=False, batch_first=self._batch_first)
			highlights_map[i] = hl
		
		return tuple(vector_x), vector_y, highlights_map
	
class HeuristicSNLICollate(StandardCollate):
	def __init__(self, padding, pipeline: NumericalizePipeline, multiclass: bool = True, batch_first: bool = True, device=None):
		assert isinstance(pipeline, NumericalizePipeline), f'Please provide the corresponding type of pipeline'
		self._device = device if device else torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
		self._pipeline = pipeline
		self._multiclass = multiclass
		self._batch_first = batch_first
		self._padding_idx = padding
		self.sent_length = list()
		
	def __call__(self, batch):
		x, y, heuristic = zip(*batch)
		
		# Let the standard collate numerize the x and y
		vector_x, vector_y = super(HeuristicSNLICollate, self).__call__(
			[(x_line, y_line) for x_line, y_line in zip(x, y)])
		heuristic = list(map(list, zip(*heuristic)))  # transpose highlight from (Nx2) into (2xN)
		heuristic = [torch.tensor(h, device=self._device) for h in heuristic]
		return tuple(vector_x), vector_y, heuristic

class TextCollate:
	"""
	Used for debugging purpose.
	"""
	
	def __init__(self, padding, pipeline, multiclass: bool = True, batch_first: bool = True):
		self._pipeline = pipeline
		self._multiclass = multiclass
		self._batch_first = batch_first
		self._padding = padding
	
	def __call__(self, batch):
		x, y = zip(*batch)
		
		x = list(map(list, zip(*x)))  # flip x == permute(1,0) dimension
		for i in range(len(x)):
			# numericalize tokens
			x[i] = self._pipeline(x[i])
			
			# add padding
			x[i] = pad_sequence(x[i], padding_value=self._padding, batch_first=self._batch_first)
		
		y = torch.LongTensor(y) if self._multiclass else torch.FloatTensor(y)
		
		return tuple(x), y