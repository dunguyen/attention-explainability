from abc import ABC


class Reporter(ABC):
	
	def __init__(self):
		pass
	
	def report_params(self, params):
		pass
	
	def report_eval(self, score):
		pass