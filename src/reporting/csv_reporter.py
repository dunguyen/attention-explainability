import os
import shutil
from os import path
from datetime import datetime
import csv
from tempfile import NamedTemporaryFile

from reporting import Reporter


class CSVReporter(Reporter):
	
	def __init__(self, out_path: str = '_out', oar_id: int = -1, script: str = 'Unknown'):
		super().__init__()
		os.makedirs(path.join(out_path, 'reports'))
		self.savepath = path.join(out_path, 'reports', 'log.csv')
		fields = ['Id', 'Script', 'Start time']
		
		if oar_id < 0:
			self.oar_id = datetime.now().strftime("%d-%m-%Y_%H:%M:%S")
		else:
			self.oar_id = str(oar_id)
		
		with open(self.savepath, 'r') as f:
			writer = csv.DictWriter(f, fieldnames=fields)
			self.report = {
				'Id': self.oar_id,
				'Script': script,
				'Start time': datetime.now().strftime("%d/%m/%Y %H:%M:%S")
			}
			writer.writerow(self.report)
		
	def report_params(self, params):
		tmp = NamedTemporaryFile(mode='w', delete=False)
		fields = ['Id', 'params']
		with open(self.savepath, 'r') as f, tmp:
			reader = csv.DictReader(f, fieldnames=fields)
			writer = csv.DictWriter(tmp, fieldnames=fields)
			for row in reader:
				if row['Id'] == self.oar_id:
					self.report.update({
						params
					})
					writer.writerow(self.report)
		shutil.move(tmp.name, self.savepath)