from argparse import ArgumentParser

from helpers import pprint, env


def parse_argument(parser_list: list, prog: str = 'train', description: str = 'Learn model of NLP'):
	assert parser_list is not None, "Have to give the list of parsers in this file"
	parser = ArgumentParser(prog=prog, description=description)
	for p in parser_list:
		parser = p(parser)
	
	params = vars(parser.parse_args())
	pprint('=== Parameters ===', params)
	
	params = {k: v for k, v in params.items() if v is not None}
	
	env.disable_tqdm = params.get('OAR_ID', None) is not None
	
	return params


def general_parser(parser: ArgumentParser):
	parser.add_argument('--data', '-d', type=str,
						help='Path to the root of dataset. '
							 'Example: "-d $HOME/dataset/snli"')
	parser.add_argument('--out', '-o', type=str, default='_out',
						help='Path to temporary directory to store output of training process. '
							'Example: "-o $HOME/_out". Default: _out')
	parser.add_argument('--cache_force', '-f', action='store_true',
						help='Flag to override existing cache resulted in output.')
	parser.add_argument('--batch_size', '-b', type=int, default=-1,
						help='Number of data in batch. Default: 32')
	parser.add_argument('--nb_data_max', '-n', type=int, default=-1,
						help='Maximum data number for train+val+test, -1 if full dataset. Default: -1')
	
	# For trainer setting
	parser.add_argument('--continuous', '-c', action='store_true',
						help='Flag to resume the previous training process, detected by model name.')
	parser.add_argument('--epoch', '-e', type=int, default=1,
						help='Number training epoch. Default: 1')
	parser.add_argument('--OAR_ID', type=int,
						help='Indicate whether we are in IGRIDA cluster mode')
	parser.add_argument('--clear_cuda', action='store_true',
						help='Whether to clear cache in each epoch. Activate will slow down a bit training but avoid overflow memory')
	return parser


def luongnet_parser(parser: ArgumentParser) -> ArgumentParser:
	parser.add_argument('--hidden', type=int, default=300,
						help='Hidden dimension and embedding dimension. Default: 300')
	parser.add_argument('--method', type=str, default='general',
						help='Score method to use. 2 available: general, dot. Default: general.')
	return parser

def simplenet_parser(parser: ArgumentParser) -> ArgumentParser:
	parser.add_argument('--hidden', type=int, default=300,
						help='Hidden dimension and embedding dimension. Default: 300')
	return parser


def snli_parser(parser: ArgumentParser) -> ArgumentParser:
	parser.add_argument('--vectors', type=str, default='glove.840B.300d',
						help='Pretrained vectors. See more in torchtext Vocab. Default: glove.840B.300d')
	parser.add_argument('--nclass', type=int, default=3,
	                    help='Limit number of class to train')
	return parser


def dataset_parser(parser: ArgumentParser) -> ArgumentParser:
	parser.add_argument('--module', '-m', type=str,
	                    help='Module that handle dataset. Available: [frnewslink, snli, snli-_bi]')
	parser.add_argument('--vectors', '-v', type=str, default='glove.840B.300d',
					help='Pretrained vectors. See more in torchtext Vocab. Default: glove.840B.300d')
	return parser


def bilstm_parser(parser: ArgumentParser) -> ArgumentParser:
	parser.add_argument('--hidden', type=int, default=200)
	return parser