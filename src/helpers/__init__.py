import pandas as pd
import torch
from tabulate import tabulate


def pprint(keys: str, display_dict: dict):
    """
    Make pretty display for a dictionary using Dataframe
    Args:
        keys: Title for pprint
        txt: Dictionary to be printed

    Returns:

    """
    print(keys)
    df = pd.DataFrame(display_dict, index=[0]).transpose()
    print(tabulate(df, tablefmt='psql'))


def hightlight_txt(txt, weights):
    """
    Build an HTML of text along its weights.
    Args:
        txt:
        weights:

    Returns: str
    Examples:
        ```python
        from IPython.core.display import display, HTML
        highlighted_text = hightlight_txt(lemma1[0], a1v2)
        display(HTML(highlighted_text))
        ```
    """
    max_alpha = 0.8

    highlighted_text = ''
    w_min, w_max = torch.min(weights), torch.max(weights)
    w_norm = (weights - w_min)/(w_max - w_min)

    for i in range(len(txt)):
        highlighted_text += '<span style="background-color:rgba(135,206,250,' \
                            + str(float(w_norm[i]) / max_alpha) + ');">' \
                            + txt[i] + '</span> '

    return highlighted_text

