import pickle
import string

import numpy as np
import torch
from torch.nn.utils.rnn import pad_sequence
from torch.utils.data import Dataset
from scipy.special import softmax
from tqdm import tqdm


def heuristic(dataset: Dataset, spacy_model, mode='softmax'):
	"""
	Compute heuristic in SNLI dataset
	Args:
		dataset ():
		spacy_model ():

	Returns:

	"""
	nb_data = len(dataset)
	
	# Note: we allow det 'the' since it'll be filtered out by 'is_stop'
	pos_filter = ['VERB', 'PROPN', 'NOUN', 'ADJ']
	m_syntax = [None] * nb_data
	m_vector = [None] * nb_data
	vectors = [None] * nb_data
	tokens = [None] * nb_data
	
	for line, data in enumerate(tqdm(dataset, desc='Tokenizing')):
		
		#x, y = data
		y = data['label']
		x = [data['premise'], data['hypothesis']]
		m_syntax[line] = [None, None]
		m_vector[line] = [None, None]
		vectors[line] = [None, None]
		tokens[line] = [None, None]
		
		for i in range(2):
			docs = spacy_model(x[i])
			# Mask if syntaxically acceptable
			syntax_mask = np.array([(not tk.is_stop) and (tk.pos_ in pos_filter) for tk in docs])
			# Mask if has vector
			vectorable_mask = np.array([tk.has_vector for tk in docs])
			
			m_syntax[line][i] = syntax_mask
			m_vector[line][i] = vectorable_mask
			
			vectors[line][i] = [tk for tk in docs]
			tokens[line][i] = [tk.text for tk in docs]
	
	# Construct similarity matrix
	sim_matrix = [None] * nb_data
	for line in range(nb_data):
		premise, hypothesis = vectors[line]
		
		# p = premise, h = hypothesis
		m = [[p.similarity(h) for h in hypothesis] for p in premise]
		
		sim_matrix[line] = np.array(m)
	
	mask_vect = [None] * nb_data
	
	for line in tqdm(range(nb_data), desc='Masking vector'):
		mask_vect[line] = [np.multiply(m_syntax[line][i], m_vector[line][i]) for i in range(2) ]
	
	def repeat_matrix(vector, repeat, axis):
		return np.repeat(np.expand_dims(vector, axis=axis), repeat, axis=axis)
	
	mask = [None] * nb_data
	for line in tqdm(range(nb_data), desc='Masking similarity matrix'):
		m1 = repeat_matrix(mask_vect[line][0], mask_vect[line][1].shape[0], axis=1).astype(float)
		m2 = repeat_matrix(mask_vect[line][1], mask_vect[line][0].shape[0], axis=0).astype(float)
		mask[line] = np.multiply(m1, m2)
	
	masked_sim = [np.multiply(mask[line], sim_matrix[line]) for line in range(nb_data)]
	
	if mode == 'softmax':
		heuristic_attention = [
			[
				softmax(masked_sim[line].sum(axis=1)),
				softmax(masked_sim[line].sum(axis=0))
			] for line in range(nb_data)
		]
	elif mode == 'sum':
		heuristic_attention = [
			[
				masked_sim[line].sum(axis=1),
				masked_sim[line].sum(axis=0)
			] for line in range(nb_data)
		]
	elif mode == 'mean':
		heuristic_attention = [
			[
				masked_sim[line].mean(axis=1),
				masked_sim[line].mean(axis=0)
			] for line in range(nb_data)
		]
	else:
		raise Exception(f'Unknown mode: {mode}')
		
	
	return tokens, heuristic_attention


def naive_heuristic(dataset: Dataset, spacy_model):
	"""
	Compute naive in SNLI dataset based only on pos_tag
	Args:
		dataset ():
		spacy_model ():

	Returns:

	"""
	# Note: we allow det 'the' since it'll be filtered out by 'is_stop'
	pos_filter = ['VERB', 'PROPN', 'NOUN', 'ADJ']
	
	texts = {'premise': list(), 'hypothesis': list()}
	
	for line, data in enumerate(tqdm(dataset, desc='Tokenizing')):
		(premise, hypothesis), y = data
		texts['premise'].append(premise)
		texts['hypothesis'].append(hypothesis)
	
	# building mask
	m_syntax = {'premise': list(), 'hypothesis': list()}
	tokens = {'premise': list(), 'hypothesis': list()}
	for side in ['premise', 'hypothesis']:
		for sentence in tqdm(spacy_model.pipe(texts[side]), desc=f'Mask for {side}', total=len(dataset)):
			
			m = [ token_.pos_ in pos_filter for token_ in sentence]
			m_syntax[side].append(m)
			
			t = [ token_.text for token_ in sentence]
			tokens[side].append(t)
	
	heuristic_attention = [m_syntax['premise'], m_syntax['hypothesis']]
	tokens = [tokens['premise'], tokens['hypothesis']]
	
	return tokens, heuristic_attention


def save(obj, fname: str):
	"""
	Save a python object with pickle
	Args:
		obj (any): a python object
		fname (str):  the file name to save. Automatically add '.pkl' at the end if does not exist

	Returns:

	"""
	if fname[-4:] != '.pkl': fname = fname + '.pkl'
	with open(fname, 'wb') as fw:
		pickle.dump(obj, fw, pickle.HIGHEST_PROTOCOL)
		print('saved at', fname)


def load(fname: str):
	"""
	Load python object with pickle
	Args:
		fname (str):  the file name to save. Automatically add '.pkl' at the end if does not exist

	Returns:
		obj (any): loaded object
	"""
	if fname[-4:] != '.pkl': fname = fname + '.pkl'
	with open(fname, 'rb') as inp:
		obj = pickle.load(inp)
	
	print('loaded at', fname)
	return obj

def cumul_dict(*dictionary):
	a = dict()
	for d in dictionary:
		for k, v in d.items():
			a[k] = a.get(k, 0) + v
	return a

def check_length(list1: list, list2: list, assertive: bool = True):
	"""
	Check length of individual list inside the 2 lists.
	Args:
		list1 (list): list of list
		list2 (list): list of list
		assertive (bool): if False, the function will return the validation state. Default at False

	Returns:

	"""
	if assertive:
		assert len(list1) == len(list2), f'Incompatible number of line: {len(list1)} vs {len(list2)}'
	if not assertive and len(list1) != len(list2): return False
	
	incoherent = [i for i in range(len(list1)) if len(list1[i]) != len(list2[i])]
	if assertive:
		assert len(incoherent) == 0, f'Incompatible vector length at {incoherent}'
		
	return len(incoherent) == 0

def extract_highlight(highlight, pipeline, get_sentence:bool = False):
	"""
	From given highlightning text, provide raw text and its attention distribution
	Args:
		highlight (list): list of texts
		pipeline (functional): function that tokenize text
	Returns:
		True if token is highlighted
	"""
	highlight = pipeline(highlight)
	masks = [None] * len(highlight)
	phrases = [None] * len(highlight)
	for idx, phrase in enumerate(highlight):
		mask = []
		normalized_phrase = []
		is_highlight = False
		for word in phrase:
			if word == '*':
				is_highlight = not is_highlight
				continue
			mask.append(is_highlight and word not in string.punctuation)
			normalized_phrase.append(word)
		masks[idx] = mask
		phrases[idx] = normalized_phrase
		
	if get_sentence:
		return masks, phrases
		
	return masks

def rescale(attentions: tuple or torch.tensor, padding_mask: tuple or list or torch.tensor=None):
	"""
	Rescale softmax attention into 0 and 1.
	Args:
		attentions ():
		padding_mask ():

	Returns:
		list of attentions or 1 side of attention
	"""
	_single = type(attentions) is not tuple
	if _single: attentions = (attentions)
	
	for side in range(len(attentions)):
		attention = attentions[side]
		
		for idx in range(len(attention)):
			a = attention[idx]
			
			if torch.all(a == 0):
				continue
				
			# if padding mask is not given, ignore the 0. at padding position
			id_unpad = ~padding_mask[side][idx] if padding_mask is not None else a > 0.
			max_value = torch.max(a[id_unpad])
			min_value = torch.min(a[id_unpad])
			
			a = (a - min_value) / (max_value - min_value + (max_value == min_value) * max_value)
			a = torch.maximum(a, torch.zeros_like(a))
			
			attentions[side][idx] = a
	
	return attentions[0] if _single else attentions
