import warnings
from os import path

from torch.utils.data import DataLoader
import spacy
import torchmetrics as m

from data.collate import *

from data.pipeline import *
from data.esnli.legacy_dataset import ExplainableESNLIDataset, build_vocab
from modules import metrics

from helpers.logger import init_logging
from helpers.argparse import *
from torchtext.vocab.vectors import pretrained_aliases as pretrained

from trainer import TrainerCallback, is_unormalize_multiclass
from model.delta import *


def batch_dot(a, b):
	return torch.bmm(a.unsqueeze(dim=1), b.unsqueeze(dim=2)).squeeze()

def regularize_attention(loss, lamb, reg_loss_fn, attentions, highlight, padding_mask=None):
	"""
	
	Args:
		loss ():
		lamb ():
		attentions ():
		highlight (tensor): human annotation
		padding_mask (tensor): 1 at padding token, 0 otherwise

	Returns:

	"""
	flat_attention = [a_[~pad_] for a_, pad_ in zip(attentions, padding_mask)]
	flat_highlight = [h_[~pad_] for h_, pad_ in zip(highlight, padding_mask)]
	
	flat_attention = torch.cat(flat_attention)
	flat_highlight = torch.cat(flat_highlight)
	flat_highlight = flat_highlight.float()
	
	return loss + lamb * reg_loss_fn(flat_attention, flat_highlight)


def parse_argument(prog: str = __name__, description: str = 'Experimentation on NLP') -> dict:
	"""
	Parse arguments passed to the script.
	Args:
		prog (str): name of the programme (experimentation)
		description (str): What do we do to this script
	Returns:
		dictionary
	"""
	parser = ArgumentParser(prog=prog, description=description)
	
	# Training params
	parser.add_argument('--data', '-d', type=str, help='Path to the root of dataset. Example: "-d $HOME/dataset/snli"')
	parser.add_argument('--cache', '-o', type=str, default='_out',
	                    help='Path to temporary directory to store output of training process')
	parser.add_argument('--nb_data_max', '-n', type=int, default=-1,
	                    help='Maximum data number for train+val+test, -1 if full dataset. Default: -1')
	parser.add_argument('--OAR_ID', type=int, help='Indicate whether we are in IGRIDA cluster mode')
	
	# For trainer setting
	parser.add_argument('--continuous', '-c', action='store_true',
	                    help='Flag to resume the previous training process, detected by model name.')
	parser.add_argument('--epoch', '-e', type=int, default=1, help='Number training epoch. Default: 1')
	parser.add_argument('--batch_size', '-b', type=int, default=32, help='Number of data in batch. Default: 32')
	parser.add_argument('--clear_cuda', action='store_true',
	                    help='Whether to clear cache in each epoch. Activate will slow down a bit training but avoid overflow memory')
	
	# Model configuration
	parser.add_argument('--suffix', type=str, default='test',
	                    help='To distinguish between different model\'s instance. Default: test')
	parser.add_argument('--vectors', type=str, default='glove.840B.300d',
	                    help='Pretrained vectors. See more in torchtext Vocab. Default: glove.840B.300d')
	parser.add_argument('--dropout', type=float)
	parser.add_argument('--activation', type=str)
	parser.add_argument('--d_embedding', type=int, default=300)
	
	# Regularization
	parser.add_argument('--lambda', type=float, default=0.)
	parser.add_argument('--entropy', action='store_true')
	parser.add_argument('--softmax', type=str, default='standard')
	parser.add_argument('--t', type=float, default=1.)
	
	params = vars(parser.parse_args())
	pprint('=== Parameters ===', params)
	params = {k: v for k, v in params.items() if v is not None}
	env.disable_tqdm = params.get('OAR_ID', None) is not None
	return params


if __name__ == '__main__':
	# Get params from command line
	params = parse_argument()
	init_logging(oar_id=params.get('OAR_ID', None), color='OAR_ID' not in params, cache_path=params.get('cache_path', '_out'))
	device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
	
	# Format params for output
	suffix: str = params.get('suffix', '')
	spacy_model = spacy.load('en_core_web_md')
	
	cache_path = params['cache']
	N = params['nb_data_max']
	batch_size = params['batch_size']
	
	# prepare dataset
	train_set = ExplainableESNLIDataset('train', cache_path, N, shuffle=True)
	val_set = ExplainableESNLIDataset('dev', cache_path, N)
	test_set = ExplainableESNLIDataset('test', cache_path, N)
	
	# define how to tokenize sentences
	text_pipeline = LemmaCasePipeline(spacy_model)
	
	# build vocab on a sub set
	vocab = build_vocab(train_set, pipeline=text_pipeline, cache_path=cache_path)
	vector_path = path.join(cache_path, 'models', '.vector_cache')
	vectors = params['vectors']  # vector string
	vectors = pretrained[vectors](cache=vector_path)
	#pretrained_embedding = vectors.get_vecs_by_tokens(vocab.get_itos())
	pretrained_vects = [vectors[token].to(device) for token in vocab.get_itos()]
	pretrained_embedding = torch.stack(pretrained_vects)
	
	# init model
	model_params = ['dropout', 'n_lstm', 'd_hidden_lstm', 'num_heads', 'n_fc_out', 'd_fc_out', 'activation']
	model_args = {k: v for k, v in params.items() if k in model_params}
	model_args.update({'n_class': len(train_set.classes)})
	
	model = DeltaSigmoidModel(vocab=vocab, d_embedding=params['d_embedding'], pretrained_embedding=pretrained_embedding, **model_args)
	modelname = model.name + '.' + suffix if suffix is not None else model.name
	
	if torch.cuda.device_count() > 1:
		log.info(f'Training on {torch.cuda.device_count()} GPUs')
		model = nn.DataParallel(model)
	else:
		log.info(f'Training on "{device}"')
	model.to(device, non_blocking=True)  # Note: move model to device before optim, parameters on GPU are different
	
	# learning configuration
	optimizer = optim.Adam(model.parameters())
	criterion = nn.CrossEntropyLoss()
	reg_loss_fn = nn.BCEWithLogitsLoss()
	callback = TrainerCallback(modelname, params['cache'], labels=train_set.classes, continuous=params['continuous'])
	
	# padding to be able to train by batch
	num_pipeline = text_pipeline.numericalizer(vocab)
	collate_fn = ExplanationESNLICollate(padding=vocab['[pad]'], pipeline=num_pipeline, tokenizer_pipeline=text_pipeline, multiclass=True, device=device)
	
	# load data into batch
	train_generator = DataLoader(train_set, collate_fn=collate_fn, batch_size=batch_size, shuffle=True)
	val_generator = DataLoader(val_set, collate_fn=collate_fn, batch_size=batch_size, shuffle=True)
	test_generator = DataLoader(test_set, collate_fn=collate_fn, batch_size=batch_size, shuffle=False)
	
	# choose metrics for reporting
	template_y_metrics = m.MetricCollection({
		'accuracy': m.Accuracy(num_classes=3),
		'f1': m.F1Score(num_classes=3)
	})
	
	template_loss_metrics = m.MetricCollection({
		'loss': m.MeanMetric()
	})
	
	with warnings.catch_warnings():
		warnings.simplefilter("ignore")
		template_attention_metrics = m.MetricCollection({
			'plausibility': m.AUROC(),
			'attention_confmat': m.ConfusionMatrix(num_classes=2)
		})
	
	SPLITS = ['train', 'val', 'test']
	
	y_metrics = {split: template_y_metrics.clone().to(device) for split in SPLITS}
	loss_metrics = {split: template_loss_metrics.clone().to(device) for split in ['train', 'val']}
	attention_metrics = {split: template_attention_metrics.clone().to(device) for split in SPLITS}
	
	# train model
	on_cuda: bool = torch.cuda.is_available()
	tqdm_arg = dict(unit='batches', file=sys.stdout, disable=env.disable_tqdm)
	
	if callback is None: callback = TrainerCallback(modelname, cache_path)
	
	# if continuous mode, check whether the checkpoint exist
	start_epoch = 0
	if params['continuous']:
		model, optimizer, start_epoch = callback.restore(model, optimizer)
		if start_epoch > 0: log.info(f'Resume training from: {callback.path["checkpoint"]}')
	else:
		log.info(f'Train new model: {modelname}')
	end_epoch = start_epoch + params['epoch']
	
	for e in range(start_epoch, end_epoch):
		# == Learning loop
		batch_train = len(train_generator)
		batch_val = len(val_generator)
		
		pbar = tqdm(total=batch_train, desc=f'Epoch {e + 1}/{end_epoch} (train)', **tqdm_arg)
		
		# === Training
		model.train()
		for id_batch, batch in enumerate(train_generator):
			
			# ==== Training batch
			
			# reset optimizer
			optimizer.zero_grad()
			
			# forward step
			x, y, hl = batch
			
			# Model computations
			preds_, attentions = model(x)
			padding_mask = [x_ == vocab['[pad]'] for x_ in x]
			
			loss = criterion(preds_, y)
			loss = regularize_attention(loss, params['lambda'], reg_loss_fn=reg_loss_fn, attentions=attentions, highlight=hl, padding_mask=padding_mask)
			
			attentions = [torch.softmax(a, dim=1) for a in attentions]
			
			if model.training and is_unormalize_multiclass(preds_):
				preds_ = torch.softmax(preds_, dim=1)
			
			for side in range(2):
				att, ann = metrics.filter_scale_attention(attentions[side], hl[side], padding_mask[side], y)
				if len(att) == 0:
					plaus = dict()
					break
				plaus = attention_metrics['train'](att, ann)
			score = {**y_metrics['train'](preds_, y.int()), **loss_metrics['train'](loss), **plaus}
			score = {k: v.item() for k, v in score.items() if k != 'attention_confmat'}
			
			# Backpropagation
			loss.backward()
			optimizer.step()
			
			# Update progress bar
			pbar.update(1)
			pbar.set_postfix(score)
			
			
			if isinstance(callback, TrainerCallback) and on_cuda:
				callback.track_gpu_memory(phase='Train memory usage', step=id_batch + e * batch_train)
				if params['clear_cuda']: torch.cuda.empty_cache()  # clean memory
		
		# ==== End training batch
		# === End Training
		
		# Update progress bar
		train_score = {**y_metrics['train'].compute(), **loss_metrics['train'].compute(), **attention_metrics['train'].compute()}
		train_score = {k: v.item() if k != 'attention_confmat' else v.tolist() for k, v in train_score.items()}
		pbar.set_postfix(train_score)
		pbar.close()
		for m in [y_metrics, loss_metrics, attention_metrics]:  m['train'].reset()
		
		# === Validation
		pbar = tqdm(total=batch_val, desc=f'Epoch {e + 1}/{end_epoch} (validation)', **tqdm_arg)
		model.eval()
		with torch.no_grad():
			
			for id_batch, batch in enumerate(val_generator):
				
				# ==== Validation batch
				x, y, hl = batch
				
				# Model computations
				preds_, attentions = model(x)
				padding_mask = [x_ == vocab['[pad]'] for x_ in x]
				
				loss = criterion(preds_, y)
				loss = regularize_attention(loss, params['lambda'], reg_loss_fn=reg_loss_fn, attentions=attentions, highlight=hl, padding_mask=padding_mask)
				
				soft_attention = [torch.softmax(a, dim=1) for a in attentions]
				
				if model.training and is_unormalize_multiclass(preds_):
					preds_ = torch.softmax(preds_, dim=1)
				
				for side in range(2):
					att, ann = metrics.filter_scale_attention(attentions[side], hl[side], padding_mask[side], y)
					if len(att) == 0:
						plaus = dict()
						break
					plaus = attention_metrics['val'](att, ann)
				score = {**y_metrics['val'](preds_, y.int()), **loss_metrics['val'](loss), **plaus}
				score = {k: v.item() for k, v in score.items() if k != 'attention_confmat'}
				
				# Update progress bar
				pbar.update(1)
				pbar.set_postfix(score)
				
				if isinstance(callback, TrainerCallback) and on_cuda:
					callback.track_gpu_memory(phase='Validation memory usage', step=id_batch + e * batch_val)
					if params['clear_cuda']: torch.cuda.empty_cache()  # Clean memory
		# ==== End validation batch
		
		# === End validation
		
		# Update progress bar
		val_score = {**y_metrics['val'].compute(), **loss_metrics['val'].compute(), **attention_metrics['val'].compute()}
		val_score = {k: v.item() if k != 'attention_confmat' else v.tolist() for k, v in val_score.items()}
		pbar.set_postfix(val_score)
		pbar.close()
		for m in [y_metrics, loss_metrics, attention_metrics]:  m['val'].reset()
		
		# In case no progressbar, print only current state
		if env.disable_tqdm:
			log.info(f'Epoch {e + 1}/{end_epoch}: (train) {train_score} | (validation) {val_score}')
			
		# Update tensorboard
		if isinstance(callback, TrainerCallback):
			callback.tensorboard({
				'Train/Loss': float(train_score['loss']),
				'Train/Accuracy': float(train_score['accuracy']),
				'Train/Plausibility': float(train_score['loss']),
				'Validation/Loss': float(val_score['loss']),
				'Validation/Accuracy': float(val_score['accuracy']),
				'Validation/Plausibility': float(val_score['loss'])
			})
			
			if callback(model, optimizer, val_score['loss']):
				log.warning(f'Early stopping at epoch {e}')
				break
		else:
			callback({
				'epoch': e,
				'train': train_score,
				'validation': val_score
			})
	
	# == End learning loop
	
	# evaluate model
	best_model = callback.best_model()
	best_model.eval()
	with torch.no_grad():
		
		pbar = tqdm(total=len(test_generator), desc='Evaluating model', unit='batches', file=sys.stdout, disable=env.disable_tqdm)
		for idx, batch in enumerate(test_generator):
			x, y, hl = batch
			
			# Model computations
			preds_, attentions = model(x)
			padding_mask = [x_ == vocab['[pad]'] for x_ in x]
			
			if model.training and is_unormalize_multiclass(preds_):
				preds_ = torch.softmax(preds_, dim=1)
			
			pbar.update(1)

			attentions = [torch.softmax(a, dim=1) for a in attentions]
			for side in range(2):
				att, ann = metrics.filter_scale_attention(attentions[side], hl[side], padding_mask[side], y)
				if len(att) == 0:
					plaus = dict()
					break
				plaus = attention_metrics['test'](att, ann)
			score = {**y_metrics['test'](preds_, y.int()), **plaus}
			score = {k: v.item() for k, v in score.items() if k != 'attention_confmat'}
			
			pbar.set_postfix(score)
		
		if callback is not None and 'confusion_matrix' in score:
			callback.tensorboard({'Evaluation/Confusion matrix': score['confusion_matrix']})
		
		test_score = {**y_metrics['test'].compute(), **attention_metrics['test'].compute()}
		test_score = {k: v.item() if k != 'attention_confmat' else v.tolist() for k, v in test_score.items()}
		pbar.set_postfix(test_score)
		pbar.close()
	
	if env.disable_tqdm: log.info(f'Model evaluation: {test_score}')
	
	if callback is not None:
		callback.report(score)
		callback.model_summary(model=model, device=device, input_data=batch[0])
	
	# clean up
	
	callback.close()
