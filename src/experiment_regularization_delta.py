from argparse import ArgumentParser

import spacy
from torch.utils.data import DataLoader

from data.collate import *

from data.pipeline import *
from data.esnli.dataset import ExplainableESNLIDataset, build_vocab
from helpers import pprint

from helpers.logger import init_logging
from trainer import TrainerCallback, Scorer, is_unormalize_multiclass
from model.delta import *
from torchtext.vocab.vectors import pretrained_aliases as pretrained
from os import path


def batch_dot(a, b):
	return torch.bmm(a.unsqueeze(dim=1), b.unsqueeze(dim=2)).squeeze()


def regularize_entropy(loss, lamb, **kwargs):
	"""
	Regularization by maximum entropy
	Args:
		loss (tensor):  compute the loss
		lamb (float): in [0,1] the influence degree into the loss function
		attention_weight (tuple): give alpha, the attention distribution along 2 sentences.

	Returns:
		Tensor
	"""
	
	alpha = kwargs.get('attention_weight', None) # tuple of 2 alpha
	N_alpha = len(alpha) # 2
	EPS = 1e-10
	log_alpha = [torch.log((alpha[i] == 0)*EPS + alpha[i]) for i in range(N_alpha)]
	reg = [batch_dot(alpha[i], log_alpha[i]) for i in range(N_alpha)]
	reg = [r.mean() for r in reg]
	return loss - lamb * (reg[0] + reg[1])


def parse_argument(prog: str = __name__, description: str = 'Experimentation on NLP') -> dict:
	"""
	Parse arguments passed to the script.
	Args:
		prog (str): name of the programme (experimentation)
		description (str): What do we do to this script
	Returns:
		dictionary
	"""
	parser = ArgumentParser(prog=prog, description=description)
	
	# Training params
	parser.add_argument('--data', '-d', type=str, help='Path to the root of dataset. Example: "-d $HOME/dataset/snli"')
	parser.add_argument('--cache', '-o', type=str, default='_out', help='Path to temporary directory to store output of training process')
	parser.add_argument('--nb_data_max', '-n', type=int, default=-1, help='Maximum data number for train+val+test, -1 if full dataset. Default: -1')
	parser.add_argument('--OAR_ID', type=int, help='Indicate whether we are in IGRIDA cluster mode')
	
	# For trainer setting
	parser.add_argument('--continuous', '-c', action='store_true', help='Flag to resume the previous training process, detected by model name.')
	parser.add_argument('--epoch', '-e', type=int, default=1, help='Number training epoch. Default: 1')
	parser.add_argument('--batch_size', '-b', type=int, default=32, help='Number of data in batch. Default: 32')
	parser.add_argument('--clear_cuda', action='store_true', help='Whether to clear cache in each epoch. Activate will slow down a bit training but avoid overflow memory')
	
	# Model configuration
	parser.add_argument('--suffix', type=str, default='test', help='To distinguish between different model\'s instance. Default: test')
	parser.add_argument('--vectors', type=str, default='glove.840B.300d', help='Pretrained vectors. See more in torchtext Vocab. Default: glove.840B.300d')
	parser.add_argument('--dropout', type=float)
	parser.add_argument('--d_embedding', type=int, default=300)
	parser.add_argument('--d_attn', type=int)
	parser.add_argument('--d_context', type=int)
	parser.add_argument('--d_fc_out', type=int)
	parser.add_argument('--n_fc_out', type=int)
	parser.add_argument('--n_lstm', type=int)
	parser.add_argument('--d_hidden_lstm', type=int)
	parser.add_argument('--activation', type=str)
	
	# Regularization
	parser.add_argument('--lambda', type=float, default=0.)
	parser.add_argument('--entropy', action='store_true')
	parser.add_argument('--softmax', type=str, default='standard')
	parser.add_argument('--t', type=float, default=1.)
	
	params = vars(parser.parse_args())
	pprint('=== Parameters ===', params)
	params = {k: v for k, v in params.items() if v is not None}
	env.disable_tqdm = params.get('OAR_ID', None) is not None
	return params

if __name__ == '__main__':
	# Get params from command line
	params = parse_argument()
	init_logging(oar_id=params.get('OAR_ID', None), color='OAR_ID' not in params, cache_path=params.get('cache_path', '_out'))
	
	# Format params for output
	suffix: str = params.get('suffix', '')
	spacy_model = spacy.load('en_core_web_sm')
	
	cache_path = params['cache']
	N = params['nb_data_max']
	batch_size = params['batch_size']
	
	# prepare dataset
	train_set = ExplainableESNLIDataset('train', cache_path, N, shuffle=True)
	val_set = ExplainableESNLIDataset('dev', cache_path, N)
	test_set = ExplainableESNLIDataset('test', cache_path, N)

	# define how to tokenize sentences
	text_pipeline = LemmaCasePipeline(spacy_model)
	
	# build vocab on a sub set
	vocab = build_vocab(train_set, pipeline=text_pipeline, cache_path=cache_path)
	vector_path = path.join(cache_path, 'models', '.vector_cache', params['vectors'])
	vectors = params['vectors'] # vector string
	vectors = pretrained[vectors](cache=vector_path)
	pretrained_embedding = vectors.get_vecs_by_tokens(vocab.get_itos())
	
	# init model
	model_params = ['dropout', 'n_lstm', 'd_hidden_lstm', 'num_heads', 'n_fc_out', 'd_fc_out', 'activation']
	model_args = {k: v for k, v in params.items() if k in model_params}
	model_args.update({'n_class': len(train_set.classes)})
	
	model = DeltaModel(vocab=vocab, d_embedding=params['d_embedding'], pretrained_embedding=pretrained_embedding, **model_args)
	modelname = model.name + '.' + suffix if suffix is not None else model.name
	
	device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
	if torch.cuda.device_count() > 1:
		log.info(f'Training on {torch.cuda.device_count()} GPUs')
		model = nn.DataParallel(model)
	else:
		log.info(f'Training on "{device}"')
	model.to(device, non_blocking=True)  # Note: move model to device before optim, parameters on GPU are different
	
	# learning configuration
	optimizer = optim.Adam(model.parameters())
	criterion = nn.CrossEntropyLoss()
	callback = TrainerCallback(modelname, params['cache'], labels=train_set.classes, continuous=params['continuous'])
	
	# padding to be able to train by batch
	num_pipeline = text_pipeline.numericalizer(vocab)
	collate_fn = ExplanationESNLICollate(padding=vocab['<pad>'], pipeline=num_pipeline, tokenizer_pipeline=text_pipeline, multiclass=True, device=device)
	
	# load data into batch
	train_generator = DataLoader(train_set, collate_fn=collate_fn, batch_size=batch_size, shuffle=True)
	val_generator = DataLoader(val_set, collate_fn=collate_fn, batch_size=batch_size, shuffle=True)
	test_generator = DataLoader(test_set, collate_fn=collate_fn, batch_size=batch_size, shuffle=False)
	
	# train model
	on_cuda: bool = torch.cuda.is_available()
	tqdm_arg = dict(unit='batches', file=sys.stdout, disable=env.disable_tqdm)
	
	if callback is None: callback = TrainerCallback(modelname, cache_path)
	
	# if continuous mode, check whether the checkpoint exist
	start_epoch = 0
	if params['continuous']:
		model, optimizer, start_epoch = callback.restore(model, optimizer)
		if start_epoch > 0: log.info(f'Resume training from: {callback.path["checkpoint"]}')
	else:
		log.info(f'Train new model: {modelname}')
	end_epoch = start_epoch + params['epoch']
	
	scoring = Scorer(metric_list=['accuracy', 'plausibility'], n_class=3)
	
	for e in range(start_epoch, end_epoch):
		# == Learning loop
		batch_train = len(train_generator)
		batch_val = len(val_generator)
		
		pbar = tqdm(total=batch_train, desc=f'Epoch {e + 1}/{end_epoch} (train)', **tqdm_arg)
		
		# === Training
		model.train()
		for id_batch, batch in enumerate(train_generator):
			
			# ==== Training batch
			
			# reset optimizer
			optimizer.zero_grad()
			
			# forward step
			x, y, hl = batch
			
			# Model computations
			preds, attentions = model(x)
			
			loss = criterion(preds, y)
			if params['entropy']:
				loss = regularize_entropy(loss, params['lambda'], attention_weight=attentions)
			
			scoring.cumulate_loss(float(loss))
			scoring.cumulate_plausibility(attentions, hl, y)
			
			# Compute scoring
			preds_ = preds.detach()  # [i] avoid storing compute graph
			
			if model.training and is_unormalize_multiclass(preds_):
				preds_ = torch.softmax(preds_, dim=1)
			
			scoring(preds_, y.int())
			
			# Update progress bar
			pbar.update(1)
			pbar.set_postfix(scoring.compute())
			
			# Backpropagation
			loss.backward()
			optimizer.step()
			
			if isinstance(callback, TrainerCallback) and on_cuda:
				callback.track_gpu_memory(phase='Train memory usage', step=id_batch + e * batch_train)
				if params['clear_cuda']: torch.cuda.empty_cache()  # clean memory
		
		# ==== End training batch
		# === End Training
		
		# Update progress bar
		epoch_score = scoring.compute()
		pbar.set_postfix(epoch_score)
		pbar.close()
		scoring.reset()
		train_loss = epoch_score['loss']
		train_acc = epoch_score['accuracy']
		train_plaus = epoch_score.get('plausibility',-1)
		
		# === Validation
		pbar = tqdm(total=batch_val, desc=f'Epoch {e + 1}/{end_epoch} (validation)', **tqdm_arg)
		model.eval()
		with torch.no_grad():
			
			for id_batch, batch in enumerate(val_generator):
				
				# ==== Validation batch
				x, y, hl = batch
				
				# Model computations
				preds, attentions = model(x)
				
				loss = criterion(preds, y)
				
				if params['entropy']:
					loss = regularize_entropy(loss, params['lambda'], attention_weight=attentions)
					
				scoring.cumulate_loss(float(loss))
				scoring.cumulate_plausibility(attentions, hl, y)
				
				# Compute scoring
				preds_ = preds.detach()  # [i] avoid storing compute graph
				
				if model.training and is_unormalize_multiclass(preds_):
					preds_ = torch.softmax(preds_, dim=1)
					
				scoring(preds_, y.int())
				
				# Update progress bar
				pbar.update(1)
				pbar.set_postfix(scoring.compute())
				
				if isinstance(callback, TrainerCallback) and on_cuda:
					callback.track_gpu_memory(phase='Validation memory usage', step=id_batch + e * batch_val)
					if params['clear_cuda']: torch.cuda.empty_cache()  # Clean memory
			# ==== End validation batch
		
		# === End validation
		
		# Update progress bar
		epoch_score = scoring.compute()
		pbar.set_postfix(epoch_score)
		pbar.close()
		scoring.reset()
		val_acc = epoch_score['accuracy']
		val_loss = epoch_score['loss']
		val_plaus = epoch_score.get('plausibility', -1)
		
		# In case no progressbar, print only current state
		if env.disable_tqdm:
			log.info(f'Epoch {e + 1}/{end_epoch}: (train) loss={train_loss}; accuracy={train_acc}; plausibility={train_plaus} | (validation) loss={val_loss}; accuracy={val_acc}; plausibility={val_plaus}')
		
		# Update tensorboard
		if isinstance(callback, TrainerCallback):
			callback.tensorboard({
				'Train/Loss': float(train_loss),
				'Train/Accuracy': float(train_acc),
				'Train/Plausibility': float(train_plaus),
				'Validation/Loss': float(val_loss),
				'Validation/Accuracy': float(val_acc),
				'Validation/Plausibility': float(val_plaus)
			})
			
			if callback(model, optimizer, val_loss):
				log.warning(f'Early stopping at epoch {e}')
				break
		else:
			callback({
				'epoch': e,
				'train': {'loss': float(train_loss), 'accuracy': float(train_acc)},
				'validation': {'loss': float(val_loss), 'accuracy': float(val_acc)}
			})
	
	# == End learning loop
	
	best_model = callback.best_model()
	
	# evaluate model
	scoring = Scorer(metric_list=['accuracy', 'precision', 'recall', 'f1', 'plausibility'], n_class=3)
	
	best_model.eval()
	with torch.no_grad():
		
		pbar = tqdm(total=len(test_generator), desc='Evaluating model', unit='batches', file=sys.stdout, disable=env.disable_tqdm)
		for idx, batch in enumerate(test_generator):
			x, y, hl = batch
			
			# Model computations
			preds, attentions = model(x)
			
			# Compute scoring
			preds_ = preds.detach()  # [i] avoid storing compute graph
			
			if model.training and is_unormalize_multiclass(preds_):
				preds_ = torch.softmax(preds_, dim=1)
			
			pbar.update(1)
			
			scoring(preds_, y.int())
			scoring.cumulate_plausibility(attentions, hl, y)
			pbar.set_postfix(scoring.compute())
		
		score = scoring.compute()
		
		if callback is not None and 'confusion_matrix' in score:
			callback.tensorboard({'Evaluation/Confusion matrix': score['confusion_matrix']})
		
		pbar.set_postfix(score)
		pbar.close()
	
	if env.disable_tqdm: log.info(f'Model evaluation: {score}')
	
	if callback is not None:
		callback.report(score)
		callback.model_summary(model=model, device=device, input_data=batch[0])
	
	# clean up
	callback.close()
