class InvalidLabelsPathException(BaseException):
    pass


class InvalidOperationError(ValueError):
    pass
