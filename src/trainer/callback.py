import json
import os
import shutil

import torch
from torch.utils.tensorboard import SummaryWriter
from torch import nn
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sn
from torchinfo import summary

from helpers import env
from helpers.logger import log


class TrainerCallback:

    def __init__(self, modelname='', path='_out', labels: list = None, patience=10, epsilon: float = 0., keep_min=True, continuous:bool=False):
        """
        Early stops the training if validation loss doesn't improve after a given patience.
        Inspired from https://github.com/Bjarten/early-stopping-pytorch/blob/master/pytorchtools.py
        Args:
            modelname: (str) Name of current model, used to restore from previous traning / save check point.
            path: (str) Where to save.
                        Callback will create 2 subdir: '/logs' for tensorboard
                        and '/models' for checkpoint and best model
                        Default: '_out'
            label: (list) list of annotated label, only used for confusion matrix
            patience: (int) How long to wait after last time validation loss improved.
                            Default: 7
            epsilon: (float) Minimum change in the monitored quantity to qualify as an improvement.
                             Default: 0
            keep_min: (bool) Save the best model when tracking value touch at min or max
                            Default: 'True'

        Usage:

            callback = TrainerCallback('AbcNet', 'out/path', patience=7, epsilon=1e-10, keep_min=True)

            # Restore learning state from previous session:
            model, optimizer, epoch = callback.restore(model, optimizer)

            # Checkpoint during learning
            if callback(model, optimizer, val_loss):
                break

            # Or manually check for early stopping
            if callback.is_stop():
                break

            # Write summary for tensorboard
            summary = {'Train/Loss': val_loss, ...}
            callback.tensorboard(summary, epoch)

            # Before ending training
            callback.close()
        """

        os.makedirs(path + '/models', exist_ok=True)
        os.makedirs(path + '/models/'+modelname, exist_ok=True)
        
        self.__bestmodel_path = os.path.join(path, 'models', modelname, 'best_model.pt')
        self.__beststate_path = os.path.join(path, 'models', modelname, 'best_state.pt')
        self.__checkpoint_path = os.path.join(path, 'models', modelname, 'checkpoint.pt')
        self.__reporting_path = os.path.join(path, 'models', modelname, 'reporting.json')
        self.__modelstats_path = os.path.join(path, 'models', modelname, 'model_summary.txt')
        
        # tensorboard path
        self.__tsb_path = os.path.join(path, 'logs', modelname)
        self.__tsb_memory_path = os.path.join(path, 'logs', modelname, 'Memory usage')

        # Save init config
        if not continuous and os.path.exists(self.__tsb_path):
            log.warning(f'Delete existing tensorboard logs at {self.__tsb_path}')
            shutil.rmtree(self.__tsb_memory_path, ignore_errors=True)
            shutil.rmtree(self.__tsb_path, ignore_errors=True)
            
        self.log_summary = SummaryWriter(self.__tsb_path)
        self.memory_summary = SummaryWriter(self.__tsb_memory_path)
        self.epsilon = epsilon
        self.patience = patience
        self.keep_min = keep_min
        self.modelname = modelname
        self.labels = labels
        self.continuous = continuous

        # Tracking variables
        self.best_score = None
        self.counter = 0
        self.epoch = 0

    def restore(self, model: nn.Module, optimizer):
        """
        Take model from previous checkpoint. Model with the same name and suffix
        Args:
            model:
            optimizer:

        Returns:

        """

        if os.path.exists(self.__checkpoint_path):
            checkpoint = torch.load(self.__checkpoint_path)
            model.load_state_dict(checkpoint['model_state_dict'])
            optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
            self.epoch = checkpoint['epoch'] + 1

        return model, optimizer, self.epoch

    def __call__(self, model, optimizer, score):

        # Save checkpoint
        torch.save({
            'epoch': self.epoch,
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict()
        }, self.__checkpoint_path)

        # Save best model
        if self.best_score is None or (self.best_score > score - self.epsilon and self.keep_min) or (
                self.best_score < score + self.epsilon and not self.keep_min):
            # Model improved
            self.best_score = score
            torch.save(model.state_dict(), self.__beststate_path)
            torch.save(model, self.__bestmodel_path) # To load the best, use best_model()
            self.counter = 0
        else:
            # Model didn't improve
            self.counter += 1

        self.epoch += 1

        return self.is_stop()

    def tensorboard(self, summary: dict, step: int=None):
        """
        Update tensorboard
        Args:
            summary: dictionary for updating informations

        """
        if step is None:
            step = self.epoch
            
        for tag, value in summary.items():
            if isinstance(value, list):
                self.draw_cfm(tag, value, step)
            else:
                self.log_summary.add_scalar(tag, value, step)
            
        self.log_summary.flush()
        
    
    def track_gpu_memory(self, phase, step: int):
        self.memory_summary.add_scalar(f'{phase}/Memory cached', torch.cuda.memory_reserved(0), step)
        self.memory_summary.add_scalar(f'{phase}/Memory allocated', torch.cuda.memory_allocated(0), step)
        
    
    def draw_cfm(self, tag, cfm: list, epoch=0):
        """
        Special method to draw confusion matrix to tensorboard
        Args:
            cfm: confusion matrix
            labels:

        Returns:

        """
        if self.labels is None or len(self.labels) < len(cfm):
            self.labels = range(len(cfm))
        if len(self.labels) > len(cfm):
            self.labels = self.labels[:len(cfm)]
        
            
        df = pd.DataFrame(cfm, index=self.labels, columns=self.labels)
        plt.figure(figsize=(10, 8), dpi=160)
        cfm_plot = sn.heatmap(df, annot=True)
        cfm_fig = cfm_plot.figure
        self.log_summary.add_figure(tag, cfm_fig, global_step=epoch, close=True)

    def is_stop(self) -> bool:
        """
        To check whether the patience is attended
        Returns:
            True if need to early stop
        """
        return self.counter > self.patience

    def close(self):
        """
        Jobs executed in post leanring phase
        """
        self.log_summary.flush()
        self.log_summary.close()

    def best_model(self) -> nn.Module:
        net = torch.load(self.__bestmodel_path)
        return net
    
    def model_summary(self, **kwargs):
        """
        Summaring model in file
        """
        kwargs['verbose'] = kwargs.get('verbose', int(not env.disable_tqdm)) # verbose = 0 if env.disable_tqdm else 1
        try:
            stats = summary(**kwargs)
            with open(self.__modelstats_path, 'w', encoding="utf-8") as f:
                f.write(str(stats))
            log.info(f'Model summary written in {self.__modelstats_path}')
        except RuntimeError or TypeError as e:
            if kwargs['input_data'] is not None:
                log.error(f'Error occured probably because of _input_data_, retry without')
                del kwargs['input_data']
                self.model_summary(**kwargs)
            else:
                log.error(f'An error occured during summary model: {e}')
        
    def report(self, score):
        """
        Make reporting file based on score
        """
        with open(self.__reporting_path, 'w') as f:
            json.dump(score, f, indent=4)
        log.info(f'Score reported in {self.__reporting_path}')

    @property
    def path(self) -> dict:
        """
        Getter for all paths
        @Exemple:
            tensorboard_path = callback.path['tensorboard']
        Returns:
            dict
        """
        return {
            'bestmodel': self.__bestmodel_path,
            'beststate': self.__beststate_path,
            'checkpoint': self.__checkpoint_path,
            'tensorboard': self.__tsb_path
        }
