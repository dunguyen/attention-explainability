# from sklearn import metrics
import warnings

import torch
import torchmetrics as metrics
from torchmetrics import functional as F

from helpers.logger import log
from torch import nn


class Scorer(nn.Module):
	
	SCALARS = ['accuracy', 'f1', 'recall', 'precision', 'plausibility', 'loss']
	MATRIX = ['confusion_matrix']

	def __init__(self, metric_list: list, n_class: int = 2, dist_sync_on_step=False):
		
		super(Scorer, self).__init__()
		
		scorers = dict()
		n_class = n_class
		for m in metric_list:
			if m == 'accuracy':
				scorers[m] = metrics.Accuracy(dist_sync_on_step=dist_sync_on_step)
			elif m == 'f1':
				scorers[m] = metrics.classification.f_beta.F1Score(n_class, average='micro',dist_sync_on_step=dist_sync_on_step)
			elif m == 'recall':
				scorers[m] = metrics.Recall(n_class, average='micro', dist_sync_on_step=dist_sync_on_step)
			elif m == 'precision':
				scorers[m] = metrics.Precision(n_class, average='micro', dist_sync_on_step=dist_sync_on_step)
			elif m == 'confusion_matrix':
				scorers[m] = metrics.ConfusionMatrix(n_class, dist_sync_on_step=dist_sync_on_step)
			elif m == 'plausibility':
				with warnings.catch_warnings():
					warnings.simplefilter("ignore")
					scorers[m] = metrics.AUROC(dist_sync_on_step=dist_sync_on_step)
			elif m == 'loss':
				scorers[m] = metrics.MeanMetric(dist_sync_on_step=dist_sync_on_step)
			else:
				print('Unknown metric:', m)
				continue
		
		self.scorers = nn.ModuleDict(scorers)
		self.loss = None
		self.n = 0

	def forward(self, y_pred: torch.tensor, y_true: torch.tensor, **kwargs) -> dict:
		"""
		Update and return all the score from different metrics
		Args:
			y_pred (tensor): 1-D of predicted probability. If multiclass this has to be softmaxed out
			y_true (tensor): 1-D of binary tensor
			attentions (tensor): (compute 'plausibility': required) 2-D array (B x L) of attentions from model
			highlights (tensor): (compute 'plausibility': required) 2-D array (B x L) of human highlights on text
			padding_mask (tensor): (compute 'plausibility': optional) 2-D array (B x L) indicating True where padding presented
		Returns:
			(dict) a dictionary of score at the update moment
		"""

		# TODO check if we can actually remove when putting scorer under Module
		# temporary solution
		#if y_true.is_cuda: y_true = y_true.cpu()
		#if y_pred.is_cuda: y_pred = y_pred.cpu()

		# score = {key: metric(y_pred, y_true) for key, metric in self.scorers.items()}
		score = dict()
		for key, metric in self.scorers.items():
			
			if key in ['plausibility']:
				
				# skip if no example match
				y_pred_arg = torch.argmax(y_pred, dim=1) if y_pred.size(1) > 1 else y_pred
				correct_preds = y_pred_arg == y_true
				
				if torch.all(y_true == 0) or (correct_preds).sum() == 0:
					continue
				
				# check key for annotations, optional: padding_mask
				self.__assert_keys(arguments=kwargs.keys(), requirements=['attentions', 'highlights'])
				
				attentions = kwargs['attentions']
				highlights = kwargs['highlights']
				padding_mask = kwargs.get('padding_mask', None)
				
				SIDES = range(2)
				padding_mask = [padding_mask[side][correct_preds] for side in SIDES]
				attentions = [attentions[side][correct_preds] for side in SIDES]
				highlights = [highlights[side][correct_preds] for side in SIDES]
				
				# rescale attentions
				for side in SIDES:
					value_max = torch.max(attentions[side] * ~padding_mask[side], dim=1, keepdim=True).values
					value_min = torch.min(attentions[side] + 1e15*padding_mask[side], dim=1, keepdim=True).values
					rescaled = (attentions[side] - value_min) / (value_max - value_min + (value_max == value_min) * value_max)
					rescaled[rescaled < 0.] = 0.
					attentions[side] = rescaled
				
				# flat at each side
				flat_attentions = [attentions[side][~padding_mask[side]] for side in SIDES]
				flat_highlights = [highlights[side][~padding_mask[side]] for side in SIDES]
				
				# flatten the 2 sides
				flat_attentions, flat_highlights = torch.cat(flat_attentions), torch.cat(flat_highlights)
				
				# Compute and cumulate
				score[key] = metric(flat_attentions, flat_highlights)
			
			elif key == 'loss':
				
				# check key for annotations, optional: padding_mask
				self.__assert_keys(arguments=kwargs.keys(), requirements=['loss'])
				loss = kwargs['loss']
				score[key] = metric(loss)
			
			else:
				score[key] = metric(y_pred, y_true)
			
		# reduce to scalar value
		for metric in self.SCALARS:
			if metric in score:
				score[metric] = float(score[metric])

		# complex score not reporting during batch
		score.pop('confusion_matrix', None)

		return score

	def __assert_keys(self, arguments: list or set, requirements: list or set):
		"""
		Check if the required key in `requirements` are presented in arguments
		Args:
			arguments ():
			requirements ():
		"""
		arguments = set(arguments)
		requirements = set(requirements)
		unmatching = requirements.intersection(arguments) - requirements
		if len(unmatching) > 0:
			raise ValueError(f'Requires following keys in arguments: {requirements}; missing: {unmatching}')
	
	def __getitem__(self, metric:str):
		"""
		Get metric scorer.
		Args:
			metric (str): the metric name

		Returns:
			scorer
		"""
		return self.scorers[metric]

	def update(self, y_pred, y_true):
		"""
		Call the update from each metrics
		Args:
			y_pred ():
			y_true ():

		Returns:

		"""
		
		# temporary solution
		if y_true.is_cuda: y_true = y_true.cpu()
		if y_pred.is_cuda: y_pred = y_pred.cpu()
		
		for k in self.scorers:
			if k in ['plausibility']: continue
			self.scorers[k].update(y_pred, y_true)

	def cumulate_loss(self, loss):
		"""
		Cumulate and averaging loss value over batches
		Args:
			loss: loss value

		Returns:
			averaging loss

		"""

		n = self.n
		if self.loss is None:
			self.loss = loss
		else:
			self.loss = self.loss*(n - 1)/n + loss/n
		self.n += 1

		return self.loss

	def compute(self):
		"""
		Average scores over epoch
		Returns:

		"""
		score = dict()
		for key, metric in self.scorers.items():
			try:
				score[key] = metric.compute()
			except ValueError as e:
				log.warn(f'No value for plausibility, affect 0 as plausibility')
				score[key] = 0.
			if key in self.SCALARS:
				score[key] = float(score[key])
			if key in self.MATRIX:
				score[key] = score[key].tolist()
		
		if self.n > 0:
			# score['loss'] = float(self.loss/self.n)
			score['loss'] = float(self.loss)

		return score
	
	def reset(self):
		for key, metric in self.scorers.items():
			metric.reset()
		self.loss = None
		self.n = 0
	

class FunctionalScorer:

	def __init__(self, metric_list: list, n_class: int = 2):

		self.scorers = dict()
		self.n_class = n_class
		for m in metric_list:
			if m == 'accuracy':
				self.scorers[m] = F.accuracy
			elif m == 'f1':
				self.scorers[m] = F.f1
			elif m == 'recall':
				self.scorers[m] = F.recall
			elif m == 'precision':
				self.scorers[m] = F.precision
			elif m == 'confusion_matrix':
				self.scorers[m] = F.confusion_matrix
			elif m == 'plausibility':
				self.scorers[m] = F.auc
			else:
				print('Unknown metric:', m)
				continue

		self.cumulative_score = {m: 0 for m in self.scorers.keys()}
		self.loss = 0.
		self.n = 0

	def __call__(self, y_pred, y_true):

		score = {key: metric(y_pred, y_true) for key, metric in self.scorers.items()}

		# reduce to scalar value
		for metric in ['accuracy', 'f1', 'recall', 'precision']:
			if metric in score:
				score[metric] = float(score[metric])
				self.cumulative_score[metric] += score[metric]

		# complex score not reporting during batch
		score.pop('confusion_matrix', None)

		return score

	def cumulate_loss(self, loss=None):
		"""
		Cumulate and averaging loss value over batches
		Args:
			loss: loss value. If not given, the scorer will reset the cumulative score

		Returns:
			averaging loss

		"""
		if loss is None:
			self.loss = 0.
			self.n = 0.
		else:
			n = self.n
			if self.loss == 0:
				self.loss = loss
			else:
				self.loss = self.loss*(n - 1)/n + loss/n
			self.n += 1


		return self.loss

	def compute(self):
		"""
		Average scores over epoch
		Returns:

		"""
		# score = {key: (metric.compute()) for key, metric in self.scorers.items()}
		score = {metric: float(metric/self.n) for metric, score in self.cumulative_score.items()}
		score['loss'] = float(self.loss)

		return score