import logging
import sys
from collections import Callable

from tqdm import tqdm
import torch

from helpers.logger import log
from helpers import env
from trainer.callback import TrainerCallback
from trainer.scorer import Scorer
from model import Net


def is_unormalize_multiclass(preds):
    """Check if prediction is not normalized"""
    return len(preds.size()) > 1 and torch.any(preds.sum(axis=1) != 1)


def forward(model, batch, scoring, loss_fn=None):
    x, y = batch
    
    # Model computations
    preds, _ = model(x)
    
    if loss_fn is not None: # evaluation doesn't need to compute loss
        loss = loss_fn(preds, y)
        scoring.cumulate_loss(float(loss))
    else:
        loss = 0
    
    # Clean memory
    del _
    
    # Compute scoring
    preds_ = preds.detach()  # [i] avoid storing compute graph
    
    if model.training and is_unormalize_multiclass(preds_):
        preds_ = model.softmax(preds_)
        

        
    score = scoring(preds_, y.int())
    
    return score, loss
    

def fit(model: Net,
        optimizer,
        loss_fn,
        device,
        epoch: int,
        generator: tuple or list,
        continuous: bool,
        callback: TrainerCallback or Callable[[dict]]=None,
        suffix:str=None,
        output_path:str='_out',
        clear_cuda=True):
    
    train_generator, val_generator = generator
    modelname = model.name + '.' + suffix if suffix is not None else model.name
    on_cuda: bool = torch.cuda.is_available()
    tqdm_arg = dict(unit='batches', file=sys.stdout, disable=env.disable_tqdm)
    
    if callback is None: callback = TrainerCallback(modelname, output_path)
    
    # if continuous mode, check whether the checkpoint exist
    start_epoch = 0
    if continuous:
        model, optimizer, start_epoch = callback.restore(model, optimizer)
        if start_epoch > 0: log.info(f'Resume training from: {callback.path["checkpoint"]}')
    else:
        log.info(f'Train new model: {modelname}')
    end_epoch = start_epoch + epoch
    
    scoring = Scorer(metric_list=['accuracy'], n_class=model.n_class)
    
    for e in range(start_epoch, end_epoch):
        # == Learning loop
        batch_train = len(train_generator)
        batch_val = len(val_generator)
        
        pbar = tqdm(total=batch_train, desc=f'Epoch {e + 1}/{end_epoch} (train)', **tqdm_arg)
        
        # === Training
        model.train()
        for id_batch, batch in enumerate(train_generator):
            
            # ==== Training batch
            
            # reset optimizer
            optimizer.zero_grad()

            #forward step
            score, loss = forward(model=model, batch=batch, loss_fn=loss_fn, scoring=scoring)
            
            # Update progress bar
            pbar.update(1)
            pbar.set_postfix(score)
            
            # Backpropagation
            loss.backward()
            optimizer.step()
            
            if isinstance(callback, TrainerCallback) and on_cuda:
                callback.track_gpu_memory(phase='Train memory usage', step=id_batch + e * batch_train)
                if clear_cuda: torch.cuda.empty_cache() # clean memory
            
            # ==== End training batch
        # === End Training
        
        # Update progress bar
        epoch_score = scoring.compute()
        pbar.set_postfix(epoch_score)
        pbar.close()
        scoring.reset()
        train_loss = epoch_score['loss']
        train_acc = epoch_score['accuracy']
        
        # === Validation
        pbar = tqdm(total=batch_val, desc=f'Epoch {e + 1}/{end_epoch} (validation)', **tqdm_arg)
        model.eval()
        with torch.no_grad():
            
            for id_batch, batch in enumerate(val_generator):
                
                # ==== Validation batch
                score, loss = forward(model=model, batch=batch, loss_fn=loss_fn, scoring=scoring)
                
                # Update progress bar
                pbar.update(1)
                pbar.set_postfix(score)

                if isinstance(callback, TrainerCallback) and on_cuda:
                    callback.track_gpu_memory(phase='Validation memory usage', step=id_batch + e*batch_val)
                    if clear_cuda: torch.cuda.empty_cache() # Clean memory
                # ==== End validation batch
        
        # === End validation
        
        # Update progress bar
        epoch_score = scoring.compute()
        pbar.set_postfix(epoch_score)
        pbar.close()
        scoring.reset()
        val_acc = epoch_score['accuracy']
        val_loss = epoch_score['loss']
        
        # In case no progressbar, print only current state
        if env.disable_tqdm:
            log.info(f'Epoch {e + 1}/{end_epoch}: (train) loss={train_loss}; accuracy={train_acc} | (validation) loss={val_loss}; accuracy={val_acc}')
        
        # Update tensorboard
        if isinstance(callback, TrainerCallback):
            callback.tensorboard({
                'Train/Loss': float(train_loss),
                'Train/Accuracy': float(train_acc),
                'Validation/Loss': float(val_loss),
                'Validation/Accuracy': float(val_acc),
            })

            if callback(model, optimizer, val_loss):
                log.warning(f'Early stopping at epoch {e}')
                break
        else:
            callback({
                'epoch': e,
                'train': {'loss': float(train_loss), 'accuracy': float(train_acc)},
                'validation': {'loss': float(val_loss), 'accuracy': float(val_acc)}
            })
        
        # == End learning loop
        
    if isinstance(callback, TrainerCallback):
        return callback.best_model()


def evaluate(model: Net, test_generator, criteria:list, device, callback: TrainerCallback or Callable[[dict]]=None) -> dict:
    """
    Evaluate a model with the best parameters
    Args:
        model:
        test_generator: must precise batch_size = N (all of data)
        criteria: list of name criteria for reporting score.
        device:
        suffix:
        output_path:

    Returns:
        dictionary of score
    """
    scoring = Scorer(metric_list=criteria, n_class=model.n_classes)
    
    model.eval()
    with torch.no_grad():
    
        bar = tqdm(total=len(test_generator), desc='Evaluating model', unit='batches', file=sys.stdout, disable=env.disable_tqdm)
        for idx, batch in enumerate(test_generator):
            
            forward(model=model, batch=batch, scoring=scoring)
            bar.update(1)
    
        score = scoring.compute()
        
        if callback is not None and 'confusion_matrix' in score:
            callback.tensorboard({'Evaluation/Confusion matrix': score['confusion_matrix']})
    
        bar.set_postfix(score)
        bar.close()
        
    if env.disable_tqdm: log.info(f'Model evaluation: {score}')
    
    if callback is not None:
        callback.report(score)
        callback.model_summary(model=model, device=device, input_data=batch[0])
    
    return score

